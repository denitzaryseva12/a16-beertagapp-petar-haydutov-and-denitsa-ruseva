package com.team13.beertag.service;

import com.team13.beertag.exception.DuplicateEntityException;
import com.team13.beertag.models.*;
import com.team13.beertag.repository.BeerRepository;
import com.team13.beertag.repository.UserRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;


import java.util.Arrays;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;


@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTests {

    @Mock
    UserRepository mockRepository;
    @InjectMocks
    UserServiceImpl mockService;

    @Test(expected = DuplicateEntityException.class)
    public void createUserShould_Throw_WhenUsernameAlreadyExist() {
        //Arrange
        Mockito.when(mockRepository.checkUsernameExists(anyString()))
                .thenReturn(true);
        //Act,Assert
        mockService.createUser(new User("testUsername", "testUsername@abv.bg"));
    }

    @Test(expected = DuplicateEntityException.class)
    public void createUserShould_Throw_WhenEmailAlreadyExist() {
        //Arrange
        Mockito.when(mockRepository.checkUsernameExists(anyString()))
                .thenReturn(true);
        //Act,Assert
        mockService.createUser(new User("testUsername", "testUsername@abv.bg"));
    }

    @Test(expected = DuplicateEntityException.class)
    public void createBeerShould_Throw_WhenBeerAlreadyExist() {
        //Arrange
        Mockito.when(mockRepository.checkUsernameExists(anyString()))
                .thenReturn(true);
        //Act,Assert
        mockService.createUser(new User("testUserName", "testUser"));
    }

    @Test
    public void createUserShould_Create_NewUser() {
        //Arrange
        User user = new User("testUsername", "testUsername@abv.bg");
        //Act
        mockService.createUser(user);
        //Assert
        Mockito.verify(mockRepository, times(1)).createUser(user);
    }

    @Test
    public void getAllUsers_Should_Return_AllUsersList_WhenHaveUsers() {
        //Arrange
        Mockito.when(mockRepository.getAllUsers()).thenReturn(Arrays.asList(
                new UserViewDto(),
                new UserViewDto(),
                new UserViewDto()));
        //Act
        mockService.getAllUsers();
        //Assert
        Assert.assertEquals(3, mockRepository.getAllUsers().size());
    }

    @Test
    public void getAllUser_Should_Return_AllUserList_WhenHaveUser() {
        //Arrange
        Mockito.when(mockRepository.getAllUsers()).thenReturn(Arrays.asList(
                new UserViewDto(),
                new UserViewDto(),
                new UserViewDto()));
        //Act
        mockService.getAllUsers();
        //Assert
        Assert.assertEquals(3, mockRepository.getAllUsers().size());
    }

    @Test
    public void updateUserShould_Update_whenUserExists() {
        //Arrange
        User user = new User("testUsername", "testUsername@abv.bg");
        User userOne = new User("testUser", "testUser@abv.bg");
        //Act
        user.setId(1);
        mockService.updateUser(1, userOne);
        //Assert
        Mockito.verify(mockRepository, times(1)).updateUser(1, userOne);
    }

    @Test
    public void deleteUser_ShouldDelete_whenExists() {
        //Arrange
        Mockito.when(mockRepository.deleteUser(1)).thenReturn(null);
        //Act
        mockService.deleteUser(1);
        //Assert
        verify(mockRepository, times(1)).deleteUser(1);
    }

    @Test
    public void deleteUser_shouldCall_Pepository() {
        //Arrange
        User user = new User("testUsername", "testUsername@abv.bg");
        //Act
        mockService.deleteUser(1);
        //Assert
        Mockito.verify(mockRepository, times(1)).deleteUser(anyInt());
    }

//    @Test
//    public void getByUsername_shouldCall_Pepository() {
//        //Arrange
//        User user=new User("testUsername", "testUsername@abv.bg");
//        //Act
//        mockService.getByUsername("testUsername");
//        //Assert
//        Mockito.verify(mockRepository, times(0)).getByUsername(anyString());
//    }

//    @Test
//    public void getUserById_ShouldReturnUser_WhenUserExists() {
//        //Arrange
//        Mockito.when(mockService.getUserById(anyInt()))
//                .thenReturn(new UserProfileDto("testUser", "testEmail@abv.bg"));
//        //Act
//        UserProfileDto result = mockService.getUserById(1);
//
//        //Assert
//        Assert.assertEquals("testUser", result.getUsername());
//    }

//    @Test
//    public void getUserById_shouldCall_Pepository() {
//        //Arrange
//        User userProfileDto=new User("Username", "testUsername@abv.bg");
//
//        Mockito.when(mockRepository.getUserById(anyInt()))
//                .thenReturn(userProfileDto);
//        //Act
//        mockService.getUserById(1);
//        //Assert
//        Mockito.verify(mockRepository,
//                times(1)).getUserById(anyInt());
//    }
//

//    @Test(expected = DuplicateEntityException.class)
//    public void updateUserShould_Throw_WhenEmailExists() {
//        //Arrange
//        User user = new User("testUsername", "testUsername@abv.bg");
//        Mockito.when(mockRepository.checkUsernameExists(anyString()))
//                .thenReturn(true);
//        Mockito.when(mockRepository.checkEmailExists(anyString()))
//                .thenReturn(true);
//        //Act,Assert
//         mockService.updateUser(0, user);
//    }

//    @Test(expected = DuplicateEntityException.class)
//    public void updateUserShould_Throw_WhenUsernameExists() {
//        //Arrange
//        User user = new User("testUsername", "testUsername@abv.bg");
//        Mockito.when(mockRepository.checkUsernameExists(anyString()))
//                .thenReturn(true);
//        Mockito.when(mockRepository.checkUsernameExists(anyString()))
//                .thenReturn(true);
//        //Act,Assert
//        mockService.updateUser(1, user);
//    }
}

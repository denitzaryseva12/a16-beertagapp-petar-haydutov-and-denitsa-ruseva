package com.team13.beertag.service;

import com.team13.beertag.exception.DuplicateEntityException;
import com.team13.beertag.models.Beer;
import com.team13.beertag.models.BeerMinimalDto;
import com.team13.beertag.models.Rating;
import com.team13.beertag.repository.BeerRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class BeerServiceImplTests {
    @Mock
    BeerRepository mockRepository;

    @InjectMocks
    BeerServiceImpl mockService;

    @Test
    public void getBeerById_ShouldReturnBeer_WhenBeerExists() {
        //Arrange
        Mockito.when(mockRepository.getById(anyInt()))
                .thenReturn(new Beer("testBeerName", "testTag", 4.6));
        //Act
        Beer result = mockService.getById(1);

        //Assert
        Assert.assertEquals("testBeerName", result.getName());
    }

    @Test
    public void getBeerById_shouldCall_Pepository() {
        //Arrange
        Beer beer = new Beer("testBeerName", "testTag", 4.6);
        Mockito.when(mockRepository.getById(anyInt())).thenReturn(beer);
        //Act
        mockService.getById(1);
        //Assert
        Mockito.verify(mockRepository, times(1)).getById(anyInt());
    }

    @Test
    public void deleteBeer_shouldCall_Pepository() {
        //Arrange
        Beer beer = new Beer("testBeerName", "testTag", 4.6);
        //Act
        mockService.delete(1);
        //Assert
        Mockito.verify(mockRepository, times(1)).delete(anyInt());
    }

    @Test(expected = DuplicateEntityException.class)
    public void createBeerShould_Throw_WhenBeerAlreadyExist() {
        //Arrange
        Mockito.when(mockRepository.checkBeerExists(anyString()))
                .thenReturn(true);
        //Act,Assert
        mockService.create(new Beer("testBeerName", "testTag", 4.6));
    }

    @Test
    public void getAllBeer_Should_Return_AllBeeryList_WhenHaveBeer() {
        //Arrange
        Mockito.when(mockRepository.getAll()).thenReturn(Arrays.asList(
                new BeerMinimalDto(),
                new BeerMinimalDto(),
                new BeerMinimalDto()));
        //Act
        mockService.getAll();
        //Assert
        Assert.assertEquals(3, mockRepository.getAll().size());
    }

    @Test
    public void getAllBeerWithFilter_Should_Return_AllBeeryList_WhenHaveBeer() {
        //Arrange
        Mockito.when(mockRepository.getAllWithFilter()).thenReturn(Arrays.asList(
                new BeerMinimalDto(),
                new BeerMinimalDto(),
                new BeerMinimalDto()));
        //Act
        mockService.getAll();
        //Assert
        Assert.assertEquals(3, mockRepository.getAllWithFilter().size());
    }

    @Test
    public void getListOfRating_Should_Return_AllBeerList_WhenHaveRating() {
        //Arrange
        Mockito.when(mockRepository.getListOfRatingsForBeer(anyInt())).thenReturn(Arrays.asList(
                new Rating(),
                new Rating(),
                new Rating()));
        //Act
        mockService.getListOfRatingsForBeer(anyInt());
        //Assert
        Assert.assertEquals(3, mockRepository.getListOfRatingsForBeer(anyInt()).size());
    }

    @Test
    public void getTopThree_Should_Return_TopThreeBeers_WhenHaveBeer() {
        //Arrange
        Mockito.when(mockRepository.getTopThreeBeers()).thenReturn(Arrays.asList(
                new BeerMinimalDto(),
                new BeerMinimalDto(),
                new BeerMinimalDto()));
        //Act
        mockService.getTopThreeBeers();
        //Assert
        Assert.assertEquals(3, mockRepository.getTopThreeBeers().size());
    }
    @Test
    public void getBeerRating_ShouldReturnRating_WhenBeerExists() {
        //Arrange
        Mockito.when(mockRepository.getBeerRating(anyInt()))
                .thenReturn(Arrays.asList(
                        new Double(0.0),
                        new Double(0.0),
                        new Double(0.0)));
        //Act
        mockService.getBeerRating(1);

        //Assert
        Assert.assertEquals(3,mockRepository.getBeerRating(1).size());
    }
}


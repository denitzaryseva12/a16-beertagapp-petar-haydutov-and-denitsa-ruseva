package com.team13.beertag.service;

import com.team13.beertag.exception.DuplicateEntityException;
import com.team13.beertag.models.Brewery;
import com.team13.beertag.repository.BreweryRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class BreweryServiceImplTests {
    @Mock
    BreweryRepository mockRepository;

    @InjectMocks
    BreweryServiceImpl mockService;

    @Test
    public void getBreweryById_ShouldReturnBrewery_WhenBreweryExists() {
        //Arrange
        Mockito.when(mockRepository.getBreweriesById(anyInt()))
                .thenReturn(new Brewery("breweryName"));
        //Act
        Brewery result = mockService.getBreweriesById(1);

        //Assert
        Assert.assertEquals("breweryName", result.getName());
    }

    @Test
    public void getBreweryById_shouldCall_Pepository() {
        //Arrange
        Brewery brewery=new Brewery("breweryName");

        Mockito.when(mockRepository.getBreweriesById(anyInt())).thenReturn(brewery);
        //Act
        mockService.getBreweriesById(1);
        //Assert
        Mockito.verify(mockRepository, times(1)).getBreweriesById(anyInt());
    }

    @Test(expected = DuplicateEntityException.class)
    public void createBreweryShould_Throw_WhenBreweryAlreadyExist() {
        //Arrange
        Mockito.when(mockRepository.checkBreweryExists(anyString()))
                .thenReturn(true);
        //Act,Assert
        mockService.
                        create(new Brewery("breweryName"));
    }

    @Test
    public void getAllBrewery_Should_Return_AllBreweryList_WhenHaveBrewery() {
        //Arrange
        Mockito.when(mockRepository.getAllBreweries()).thenReturn(Arrays.asList(
                new Brewery(),
                new Brewery(),
                new Brewery()));
        //Act
        mockService.getAllBreweries();
        //Assert
        Assert.assertEquals(3, mockRepository.getAllBreweries().size());
    }

    @Test(expected = DuplicateEntityException.class)
    public void updateBreweryShould_Throw_WhenBreweryExists() {
        //Arrange
        Brewery brewery=new Brewery("breweryName");
        Mockito.when(mockRepository.checkBreweryExists(anyString()))
                .thenReturn(true);
        //Act,Assert
        mockService.update(1, brewery);
    }
    @Test
    public void deleteBrewery_shouldCall_Pepository() {
        //Arrange
        Brewery brewery=new Brewery("breweryName");

        //Act
        mockService.delete(1);
        //Assert
        Mockito.verify(mockRepository, times(1)).delete(anyInt());
    }
}


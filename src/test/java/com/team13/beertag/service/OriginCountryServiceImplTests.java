package com.team13.beertag.service;

import com.team13.beertag.exception.DuplicateEntityException;
import com.team13.beertag.models.OriginCountry;
import com.team13.beertag.repository.OriginCountryRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class OriginCountryServiceImplTests {

    @Mock
    OriginCountryRepository mockRepository;

    @InjectMocks
    OriginCountryServiceImpl mockService;

    @Test
    public void getCountryById_ShouldReturnCountry_WhenCountryExists() {
        //Arrange
        Mockito.when(mockRepository.getCountryById(anyInt()))
                .thenReturn(new OriginCountry("originCountryName"));
        //Act
        OriginCountry result = mockService.getById(1);

        //Assert
        Assert.assertEquals("originCountryName", result.getName());
    }

    @Test
    public void getCountryById_shouldCall_Pepository() {
        //Arrange
        OriginCountry country = new OriginCountry("originCountryName");

        Mockito.when(mockRepository.getCountryById(anyInt())).thenReturn(country);
        //Act
        mockService.getById(1);
        //Assert
        Mockito.verify(mockRepository, times(1)).getCountryById(anyInt());
    }

    @Test(expected = DuplicateEntityException.class)
    public void createCountryShould_Throw_WhenCountryAlreadyExist() {
        //Arrange
        Mockito.when(mockRepository.checkCountryExists(anyString()))
                .thenReturn(true);
        //Act,Assert
         mockService.
                        create(new OriginCountry("originCountryName"));
    }

    @Test
    public void getAllCountry_Should_Return_AllCountryList_WhenHaveCountry() {
        //Arrange
        Mockito.when(mockRepository.getAll()).thenReturn(Arrays.asList(
                new OriginCountry(),
                new OriginCountry(),
                new OriginCountry()));
        //Act
        mockService.getAll();
        //Assert
        Assert.assertEquals(3, mockRepository.getAll().size());
    }

    @Test(expected = DuplicateEntityException.class)
    public void updateCountryShould_Throw_WhenCountryExists() {
        //Arrange
        OriginCountry country = new OriginCountry("originCountryName");
        Mockito.when(mockRepository.checkCountryExists(anyString()))
                .thenReturn(true);
        //Act,Assert
        mockService.update(1, country);
    }

    @Test
    public void deleteCountry_shouldCall_Pepository() {
        //Arrange
        OriginCountry country = new OriginCountry("originCountryName");

        //Act
        mockService.delete(1);
        //Assert
        Mockito.verify(mockRepository, times(1)).delete(anyInt());
    }
}

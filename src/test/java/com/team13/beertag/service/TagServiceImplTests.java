package com.team13.beertag.service;

import com.team13.beertag.exception.DuplicateEntityException;
import com.team13.beertag.models.Tag;
import com.team13.beertag.repository.TagRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class TagServiceImplTests {

    @Mock
    TagRepository mockRepository;

    @InjectMocks
    TagServiceImpl mockService;

    @Test
    public void getTagById_ShouldReturnTag_WhenTagExists() {
        //Arrange
        Mockito.when(mockRepository.getTagById(anyInt()))
                .thenReturn(new Tag("tagName"));
        //Act
        Tag result = mockService.getTagById(1);

        //Assert
        Assert.assertEquals("tagName", result.getName());
    }

    @Test
    public void getTagById_shouldCall_Pepository() {
        //Arrange
        Tag tag = new Tag("tagName");

        Mockito.when(mockRepository.getTagById(anyInt())).thenReturn(tag);
        //Act
        mockService.getTagById(1);
        //Assert
        Mockito.verify(mockRepository, times(1)).getTagById(anyInt());
    }

    @Test(expected = DuplicateEntityException.class)
    public void createTagShould_Throw_WhenTagAlreadyExist() {
        //Arrange
        Mockito.when(mockRepository.checkTagExists(anyString()))
                .thenReturn(true);
        //Act,Assert
        mockService.
                        create(new Tag("tagName"));
    }

    @Test
    public void getAllTags_Should_Return_AllTagsList_WhenHaveTag() {
        //Arrange
        Mockito.when(mockRepository.getAll()).thenReturn(Arrays.asList(
                new Tag(),
                new Tag(),
                new Tag()));
        //Act
        mockService.getAll();
        //Assert
        Assert.assertEquals(3, mockRepository.getAll().size());
    }

    @Test(expected = DuplicateEntityException.class)
    public void updateTagShould_Throw_WhenTagExists() {
        //Arrange
        Tag tag = new Tag("tagName");
        Mockito.when(mockRepository.checkTagExists(anyString()))
                .thenReturn(true);
        //Act,Assert
        mockService.update(1, tag);
    }

    @Test
    public void deleteTag_ShouldDelete_whenExists() {
        //Arrange
        Mockito.when(mockRepository.deleteTag(1)).thenReturn(null);
        //Act
        mockService.delete(1);
        //Assert
        Mockito.verify(mockRepository, times(1)).deleteTag(1);
    }

    @Test
    public void deleteTag_shouldCall_Pepository() {
        //Arrange
        Tag tag = new Tag("tagName");
        //Act
        mockService.delete(1);
        //Assert
        Mockito.verify(mockRepository, times(1)).deleteTag(anyInt());
    }
    @Test
    public void filterTags_Should_Return_FilteredTags_WhenHaveTag() {
        //Arrange
        Mockito.when(mockRepository.filterByName(anyString()))
                .thenReturn(Arrays.asList(
                new Tag(),
                new Tag(),
                new Tag()));
        //Act
        mockService.filterByName("filterTest");
        //Assert
        Assert.assertEquals(3, mockRepository.filterByName("filterTest").size());
    }
}

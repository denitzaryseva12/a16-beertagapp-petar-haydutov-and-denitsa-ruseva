package com.team13.beertag.service;

import com.team13.beertag.exception.DuplicateEntityException;
import com.team13.beertag.exception.EntityNotFoundException;
import com.team13.beertag.models.Style;
import com.team13.beertag.repository.StyleRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class StyleServiceImplTests {

    @Mock
    StyleRepository mockRepository;

    @InjectMocks
    StyleServiceImpl mockService;

    @Test
    public void getStyleById_ShouldReturnStyle_WhenStyleExists() {
        //Arrange
        Mockito.when(mockRepository.getStylesById(anyInt()))
                .thenReturn(new Style("styleName"));
        //Act
        Style result = mockService.getStylesById(1);

        //Assert
        Assert.assertEquals("styleName", result.getName());
    }

    @Test
    public void getStyleById_shouldCall_Pepository() {
        //Arrange
        Style style = new Style("styleName");

        Mockito.when(mockRepository.getStylesById(anyInt())).thenReturn(style);
        //Act
        mockService.getStylesById(1);
        //Assert
        Mockito.verify(mockRepository, times(1)).getStylesById(anyInt());
    }

    @Test(expected = DuplicateEntityException.class)
    public void createStyleShould_Throw_WhenStyleAlreadyExist() {
        //Arrange
        Mockito.when(mockRepository.checkStyleExists(anyString()))
                .thenReturn(true);
        //Act,Assert
        mockService.
                        create(new Style("styleName"));
    }

    @Test
    public void getAllStyle_Should_Return_AllStylesList_WhenHaveStyle() {
        //Arrange
        Mockito.when(mockRepository.getAll()).thenReturn(Arrays.asList(
                new Style(),
                new Style(),
                new Style()));
        //Act
        mockService.getAllStyles();
        //Assert
        Assert.assertEquals(3, mockRepository.getAll().size());
    }

    @Test(expected = DuplicateEntityException.class)
    public void updateStyleShould_Throw_WhenStyleExists() {
        //Arrange
        Style style = new Style("styleName");
        Mockito.when(mockRepository.checkStyleExists(anyString()))
                .thenReturn(true);
        //Act,Assert
        mockService.update(1, style);
    }

    @Test
    public void deleteStyle_shouldCall_Pepository() {
        //Arrange
        Style style = new Style("styleName");
        //Act
        mockService.delete(1);
        //Assert
        Mockito.verify(mockRepository, times(1)).delete(anyInt());
    }
    @Test
    public void getByStyleName_Should_Return_styleName_WhenHaveStyle() {
        //Arrange
        Mockito.when(mockRepository.getByStyleName(anyString()))
                .thenReturn(Arrays.asList(
                        new Style(),
                        new Style(),
                        new Style()));
        //Act
        mockService.getByStyleName("styleTest");
        //Assert
        Assert.assertEquals(3, mockRepository.getByStyleName("styleTest").size());
    }
}

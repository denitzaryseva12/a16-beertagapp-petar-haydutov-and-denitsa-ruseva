package com.team13.beertag.constants;

public class UserConstants {
    //Length constants
    //Username
    public static final int USER_NAME_MIN_LENGTH = 4;
    public static final int USER_NAME_MAX_LENGTH = 50;
    public static final String USERNAME_INVALID_LENGTH_ERROR_MSG =
            "The Username's length should be between {min} and {max} symbols.";
    public static final String USERNAME_EXISTS_MESSAGE = "User with username %s already exists.";

    //Password
    public static final int PASSWORD_MIN_LENGTH = 6;
    public static final int PASSWORD_MAX_LENGTH = 68;
    public static final String PASSWORD_INVALID_LENGTH_ERROR_MSG = "The Password should be between {min} and {max} symbols.";

    //Email
    public static final int EMAIL_MIN_LENGTH = 6;
    public static final int EMAIL_MAX_LENGTH = 30;
    public static final String EMAIL_INVALID_LENGTH_ERROR_MSG = "Email should be between {min} and {max} symbols.";
    public static final String EMAIL_EXISTS_MESSAGE = "User with email %s already exists.";

    //Error message constants
    public static final String USER_NOT_FOUND = "User with id %d does not exist";
    public static final String USER_HAS_NO_DRUNK_LIST = "User with id %d does not have any beers in his drunk list";
    public static final String WISHLIST_ALREADY_CONTAINS_BEER = "This Wish List already contains the beer %s";
    public static final String DRANK_LIST_ALREADY_CONTAINS_BEER = "This Drank List already contains the beer %s";

    //Role
    public static final String ROLE_NOT_FOUND = "Role with id %d not found";
    public static final String USER_ROLE_ALREADY_ASSIGNED = "User %s already is assigned as %s";

}

package com.team13.beertag.constants;

public class BeerConstants {
    //Beer
    public static final int BEER_NAME_MIN_LENGTH = 2;
    public static final int BEER_NAME_MAX_LENGTH = 50;
    public static final String BEER_NOT_FOUND = "Beer with id %d does not exist.";
    public static final String POSITIVE_BEER_ID = "Beer id should be positive.";
    public static final String BEER_EXISTS_MESSAGE = "Beer with name %s already exists.";
    public static final String BEER_INVALID_LENGTH_ERROR_MSG =
            "The beer name's length should be between {min} and {max} symbols.";
    public static final String BEER_NAME_NOT_FOUND="Beer with name %s does not exist.";
//    public static final String RATING_NOT_IN_RANGE="Rating should between %d and %d.";
    public static final double RATING_MIN = 0.1;
    public static final double RATING_MAX= 5.0;

    //Description
    public static final int DESCRIPTION_NAME_MIN_LENGTH = 21;
    public static final int DESCRIPTION_NAME_MAX_LENGTH = 500;
    public static final String DESCRIPTION_INVALID_LENGTH_ERROR_MSG =
            "The description's length should be between {min} and {max} symbols.";

    //Origin Country
    public static final int ORIGIN_COUNTRY_NAME_MIN_LENGTH = 4;
    public static final int ORIGIN_COUNTRY_MAX_LENGTH = 50;
    public static final String ORIGIN_COUNTRY_EXISTS_MESSAGE = "Country with name %s already exists.";
    public static final String ORIGIN_COUNTRY_INVALID_NAME_LENGTH_ERROR_MSG =
            "The Country name must be between {min} and {max} symbols long";
    public static final String POSITIVE_ORIGIN_COUNTRY_ID = "Origin CountryID should be positive.";

    //Brewery
    public static final int BREWERY_NAME_MIN_LENGTH = 3;
    public static final int BREWERY_NAME_MAX_LENGTH = 50;
    public static final String BREWERY_EXISTS_MESSAGE = "Brewery with name %s already exists.";
    public static final String BREWERY_INVALID_NAME_LENGTH_ERROR_MSG =
            "The Brewery's name must be between {min} and {max} symbols long";
    public static final String POSITIVE_BREWERY_ID = "BreweryID should be positive.";
    ;

    //Style
    public static final int STYLE_NAME_MIN_LENGTH = 3;
    public static final int STYLE_NAME_MAX_LENGTH = 50;
    public static final String STYLE_EXISTS_MESSAGE = "Style with name %s already exists.";
    public static final String STYLE_INVALID_NAME_LENGTH_ERROR_MSG =
            "The Style's name must be between {min} and {max} symbols long.";
    public static final String POSITIVE_STYLE_ID = "StyleID should be positive.";

    //Tag
    public static final int TAG_NAME_MIN_LENGTH = 3;
    public static final int TAG_NAME_MAX_LENGTH = 50;
    public static final String TAG_EXISTS_MESSAGE = "Tag with name %s already exists.";
    public static final String TAG_INVALID_NAME_LENGTH_ERROR_MSG =
            "The Tag's name must be between {min} and {max} symbols long.";
    public static final String POSITIVE_TAG_ID = "TagID should be positive.";

    //Error message
    public static final String TAG_NOT_FOUND = "Tag with id %d does not exist";
    public static final String TAG_NAME_NOT_FOUND = "Tag with name %s does not exist";
    public static final String STYLE_NOT_FOUND = "Style with id %d does not exist";
    public static final String STYLE_NAME_NOT_FOUND = "Style with name %s does not exist";
    public static final String ORIGIN_COUNTRY_NOT_FOUND = "Country with id %d does not exist";
    public static final String ORIGIN_COUNTRY_NAME_NOT_FOUND = "Country with name %s does not exist";
    public static final String BREWERY_NOT_FOUND = "Brewery with id %d does not exist";
    public static final String BREWERY_NAME_NOT_FOUND = "Brewery with name %s does not exist";
    public static final String PAGE_DO_NO_EXIST_MESSAGE="Page does not exist.";

    //Rating
    public static final String BEER_ALREADY_RATED_BY_USER = "User with id %d has already rated beer with id %d";

}


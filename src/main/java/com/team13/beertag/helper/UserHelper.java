package com.team13.beertag.helper;

import com.team13.beertag.models.User;
import com.team13.beertag.models.UserViewDto;

public class UserHelper {
    public static User updateUserDetails(User user, UserViewDto userViewDto){
        user.setEmail(userViewDto.getEmail());
        user.setFirstName(userViewDto.getFirstName());
        user.setLastName(userViewDto.getLastName());
        user.setPicture(userViewDto.getPicture());

        return user;
    }
}

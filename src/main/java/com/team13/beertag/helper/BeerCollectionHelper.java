package com.team13.beertag.helper;

import com.team13.beertag.models.Beer;
import com.team13.beertag.models.BeerDto;
import com.team13.beertag.models.BeerMinimalDto;
import com.team13.beertag.service.TagService;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class BeerCollectionHelper {
    public static Beer updateBeerDetails(Beer beerToUpdate, BeerDto beerDto, TagService tagService) {
        beerToUpdate.setAbv(beerDto.getAbv() != 0 ? beerDto.getAbv() : beerToUpdate.getAbv());
        beerToUpdate.setDescription(!beerDto.getDescription().isEmpty() ? beerDto.getDescription() : beerToUpdate.getDescription());
        beerToUpdate.setName(!beerDto.getName().isEmpty() ? beerDto.getName() : beerToUpdate.getName());
        if(!beerToUpdate.getTags().contains(tagService.getByName(beerDto.getTags().get(0)))) {
            beerToUpdate.addTag(tagService.getByName(beerDto.getTags().get(0)));
        }

        return beerToUpdate;
    }
}
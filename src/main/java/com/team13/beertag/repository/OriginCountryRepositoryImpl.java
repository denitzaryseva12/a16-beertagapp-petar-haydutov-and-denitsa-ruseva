package com.team13.beertag.repository;


import com.team13.beertag.exception.EntityNotFoundException;
import com.team13.beertag.models.OriginCountry;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static com.team13.beertag.constants.BeerConstants.ORIGIN_COUNTRY_NAME_NOT_FOUND;
import static com.team13.beertag.constants.BeerConstants.ORIGIN_COUNTRY_NOT_FOUND;

@Repository
public class OriginCountryRepositoryImpl implements OriginCountryRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public OriginCountryRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    public List<OriginCountry> getAll() {
        Session session = sessionFactory.getCurrentSession();
        Query<OriginCountry> query=session.createQuery("from OriginCountry where enabled = true " +
                "order by id",OriginCountry.class);
        return query.list();
    }

    @Override
    public OriginCountry getCountryById(int id) {
        Session session = sessionFactory.getCurrentSession();
        OriginCountry country = session.createQuery(
                "from OriginCountry oc where oc.id = :id", OriginCountry.class)
                .setParameter("id", id)
                .uniqueResult();
        if (country == null) {
            throw new EntityNotFoundException(String.format(ORIGIN_COUNTRY_NOT_FOUND, id));
        }
        return country;
    }

    @Override
    public OriginCountry create(OriginCountry originCountry) {
        try (Session session = sessionFactory.openSession()) {
            session.save(originCountry);
        }
        return originCountry;
    }

    @Override
    public boolean checkCountryExists(String name) {
        return getByOriginCountryName(name).size() != 0;
    }

    private List<OriginCountry> getByOriginCountryName(String name) {
        Session session = sessionFactory.getCurrentSession();
        Query<OriginCountry> query = session.createQuery(
                "from OriginCountry where name = :name order by id",
                OriginCountry.class);
        query.setParameter("name", name);
        if (query == null) {
            throw new EntityNotFoundException(String.format(ORIGIN_COUNTRY_NAME_NOT_FOUND, name));
        }
        return query.list();
    }

    @Override
    public void delete(int id) {
        OriginCountry countryToUpdate = getCountryById(id);
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        countryToUpdate.setEnabled(false);
        session.update(countryToUpdate);
        session.getTransaction().commit();
    }

    @Override
    public OriginCountry update(int id, OriginCountry originCountry) {
        OriginCountry countryToUpdate = getCountryById(id);
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        if (originCountry.getName() != null) {
            countryToUpdate.setName(originCountry.getName());
        }
        session.update(countryToUpdate);
        session.getTransaction().commit();
        return countryToUpdate;
    }
}

package com.team13.beertag.repository;

import com.team13.beertag.models.*;

import java.util.List;
import java.util.Set;

public interface UserRepository {
    User createUser(User user);

    List<UserViewDto> getAllUsers();

    User updateUser(int id, User user);

    User deleteUser(int id);

    User getUserById(int id);

    boolean checkUsernameExists(String username);

    boolean checkEmailExists(String email);

    Set<Beer> addBeerToDrunkList(int userId, int beerId);

    Set<Beer> addBeerToWishList(int userId, int beerId);

    List<User> getByUsername(String username);

    List<User> getByEmail(String email);
}

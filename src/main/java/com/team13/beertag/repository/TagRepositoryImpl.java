package com.team13.beertag.repository;

import com.team13.beertag.exception.EntityNotFoundException;
import com.team13.beertag.models.Tag;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import static com.team13.beertag.constants.BeerConstants.*;

@Repository
public class TagRepositoryImpl implements TagRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public TagRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Tag> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Tag> query = session.createQuery(
                    "from Tag where enabled = true order by id", Tag.class);
            return query.list();
        }
    }

    @Override
    public List<Tag> filterByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Tag> query = session.createQuery(
                    "from Tag where enabled = true and name like :name", Tag.class);
            query.setParameter("name", "%" + name + "%");
            return query.list();
        }
    }

    @Override
    public Tag  getTagById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Tag tag = session.get(Tag.class, id);
            if (tag == null) {
                throw new EntityNotFoundException(String.format(TAG_NOT_FOUND, id));
            }
            return tag;
        }
    }

    @Override
    public Tag createTag(Tag tag) {
        try (Session session = sessionFactory.openSession()) {
            session.save(tag);
        }
        return tag;
    }

    @Override
    public Tag updateTag(int id, Tag tag) {
        Tag tagToBeUpdated = getTagById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            tagToBeUpdated.setName(tag.getName());
            session.update(tagToBeUpdated);
            session.getTransaction().commit();
        }
        return tagToBeUpdated;
    }

    @Override
    public Tag deleteTag(int id) {
        Tag tagToBeDeleted = getTagById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            tagToBeDeleted.setEnabled(false);
            session.update(tagToBeDeleted);
            session.getTransaction().commit();
        }
        return tagToBeDeleted;
    }

    @Override
    public boolean checkTagExists(String name) {
        return getByName(name).size() != 0;
    }

    private List<Tag> getByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Tag> query = session.createQuery("from Tag where name like :name", Tag.class);
            query.setParameter("name", name);
            if (query == null) {
                throw new EntityNotFoundException(String.format(TAG_NAME_NOT_FOUND, name));
            }
            return query.list();
        }
    }

    @Override
    public Tag getTagByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Tag> query = session.createQuery("from Tag where name like :name", Tag.class).
                setParameter("name", "%" + name + "%");
            if (query == null) {
                throw new EntityNotFoundException(String.format(TAG_NAME_NOT_FOUND, name));
            }
            return query.list().get(0);
        }
    }
}

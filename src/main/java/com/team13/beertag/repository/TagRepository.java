package com.team13.beertag.repository;

import com.team13.beertag.models.Tag;

import java.util.List;

public interface TagRepository {
    List<Tag> getAll();

    Tag getTagById(int id);

    Tag createTag(Tag tag);

    boolean checkTagExists(String content);

    Tag deleteTag(int id);

    Tag updateTag(int id, Tag tag);

    List<Tag> filterByName(String name);

    Tag getTagByName(String name);
}

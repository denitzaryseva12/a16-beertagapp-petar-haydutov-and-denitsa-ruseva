package com.team13.beertag.repository;

import com.team13.beertag.models.Beer;
import com.team13.beertag.models.BeerMinimalDto;
import com.team13.beertag.models.BeerViewListDto;
import com.team13.beertag.models.Rating;

import java.util.List;
import java.util.Set;

public interface BeerRepository {
    Beer create(Beer Beer);

    List<BeerMinimalDto> getAllWithFilter();

    List<BeerMinimalDto> getAll();

    List<Beer> getBeerByName(String name);

    boolean checkBeerExists(String name);

    Beer update(int id, Beer beer);

    void delete(int id);

    Beer getById(int id);

    List<BeerMinimalDto> filterByCountry(String originCountry);

    List<BeerMinimalDto> filterByStyle(String style);

    List<BeerMinimalDto> filterByTag(String tag);

    List<BeerMinimalDto> sortByName();

    List<BeerMinimalDto> sortByAbvAsc();

    List<BeerMinimalDto> sortByAbvDesc();

    List<BeerMinimalDto> getTopThreeBeers();

    List<BeerMinimalDto> sortByRatingAsc();

    List<BeerMinimalDto> sortByRatingDesc();

    Double addBeerRating(int id, Rating rating);

    List<Double> getBeerRating(int id);

    Set<BeerViewListDto> addRatingToBeerViewListDto(Set<BeerViewListDto> list);

    List<Rating> getListOfRatingsForBeer(int id);

}

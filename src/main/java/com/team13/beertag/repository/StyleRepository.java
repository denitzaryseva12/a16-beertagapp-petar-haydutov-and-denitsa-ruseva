package com.team13.beertag.repository;

import com.team13.beertag.models.Style;

import java.util.List;

public interface StyleRepository {
    List<Style> getAll();

    Style getStylesById(int id);

    Style create(Style style);

    boolean checkStyleExists(String name);

    void delete(int id);

    Style update(int id, Style style);

    List<Style> getByStyleName(String name);
}

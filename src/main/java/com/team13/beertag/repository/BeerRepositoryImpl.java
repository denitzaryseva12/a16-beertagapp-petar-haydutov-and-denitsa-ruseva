package com.team13.beertag.repository;

import com.team13.beertag.exception.DuplicateEntityException;
import com.team13.beertag.exception.EntityNotFoundException;
import com.team13.beertag.models.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.*;
import java.util.stream.Collectors;

import static com.team13.beertag.constants.BeerConstants.*;

@Repository
public class BeerRepositoryImpl implements BeerRepository {
    private static final int BEERS_PER_PAGE = 6;
    private SessionFactory sessionFactory;
    private DtoMapper dtoMapper;

    @Autowired
    public BeerRepositoryImpl(SessionFactory sessionFactory, DtoMapper dtoMapper) {
        this.sessionFactory = sessionFactory;
        this.dtoMapper = dtoMapper;
    }

    @Override
    public Beer create(Beer beer) {
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        session.save(beer);
        session.getTransaction().commit();
        return beer;
    }

    @Override
    public List<BeerMinimalDto> getAllWithFilter() {
        Session session = sessionFactory.getCurrentSession();
        List<BeerMinimalDto> beer = session.createQuery(
                "select distinct new com.team13.beertag.models.BeerMinimalDto(" +
                        "b.id, b.name,b.description,b.abv, b.style.name, b.originCountry.name, b.brewery.name, b.picture) " +
                        "from Beer b " +
                        "where b.enabled = true", BeerMinimalDto.class).list();
        return beer;
    }

    @Override
    public List<BeerMinimalDto> getAll() {
        Session session = sessionFactory.getCurrentSession();
        List<BeerMinimalDto> beer = session.createQuery(
                "select distinct new com.team13.beertag.models.BeerMinimalDto(" +
                        "b.id, b.name,b.description,b.abv, b.style.name, b.originCountry.name, b.brewery.name, b.picture) " +
                        "from Beer b " +
                        "where b.enabled = true", BeerMinimalDto.class).list();
        addRatingForAllBeers(beer);
        return beer;
    }

    @Override
    public boolean checkBeerExists(String name) {
        return getBeerByName(name).size() != 0;
    }

    @Override
    public List<Beer> getBeerByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Beer> query = session.createQuery(
                    "from Beer where name = :name and enabled = true order by id",
                    Beer.class);
            query.setParameter("name", name);
            if (query == null) {
                throw new EntityNotFoundException(String.format(BEER_NAME_NOT_FOUND, name));
            }
            return query.list();
        }
    }

    @Override
    public Beer update(int id, Beer beer) {
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        Beer beerToUpdate = getById(id);
        if (beer.getName() != null) {
            beerToUpdate.setName(beer.getName());
        }
        if (beer.getAbv() != 0) {
            beerToUpdate.setAbv(beer.getAbv());
        }
        if (beer.getDescription() != null) {
            beerToUpdate.setDescription(beer.getDescription());
        }
        if (beer.getOriginCountry() != null) {
            beerToUpdate.setOriginCountry(beer.getOriginCountry());
        }
        if (beer.getBrewery() != null) {
            beerToUpdate.setBrewery(beer.getBrewery());
        }
        if (beer.getStyle() != null) {
            beerToUpdate.setStyle(beer.getStyle());
        }
        session.update(beerToUpdate);
        session.getTransaction().commit();
        return beerToUpdate;
    }

    @Override
    public void delete(int id) {
        Beer beerToUpdate = getById(id);
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        beerToUpdate.setEnabled(false);
        session.update(beerToUpdate);
        session.getTransaction().commit();

    }

    @Override
    public Beer getById(int id) {
        Session session = sessionFactory.getCurrentSession();
        Beer beer = session.createQuery("from Beer b where b.id = :id " +
                "and enabled = true", Beer.class)
                .setParameter("id", id)
                .uniqueResult();
        if (beer == null) {
            throw new EntityNotFoundException(String.format(BEER_NOT_FOUND, id));
        }
        return beer;
    }

    @Override
    public List<BeerMinimalDto> filterByCountry(String originCountry) {
        Session session = sessionFactory.getCurrentSession();
        List<BeerMinimalDto> listOfBeers = session.createQuery(
                "select distinct new com.team13.beertag.models.BeerMinimalDto(" +
                        "b.id, b.name,b.description,b.abv, b.style.name, b.originCountry.name, b.brewery.name) " +
                        "from Beer b " +
                        "where b.originCountry.name = :originCountry " +
                        "and b.enabled = true", BeerMinimalDto.class)
                .setParameter("originCountry", originCountry)
                .list();
        return listOfBeers;
    }

    @Override
    public List<BeerMinimalDto> filterByStyle(String style) {
        Session session = sessionFactory.getCurrentSession();
        List<BeerMinimalDto> listOfBeers = session.createQuery(
                "select distinct new com.team13.beertag.models.BeerMinimalDto(" +
                        "b.id, b.name,b.description,b.abv, b.style.name, b.originCountry.name, b.brewery.name) " +
                        "from Beer b " +
                        "where b.style.name like :style " +
                        "and b.enabled = true", BeerMinimalDto.class)
                .setParameter("style", "%" + style + "%")
                .list();
        return listOfBeers;
    }

    @Override
    public List<BeerMinimalDto> filterByTag(String tag) {
        Session session = sessionFactory.getCurrentSession();
        List<Beer> listOfBeers = session.createQuery(
                "from Beer b where b.enabled = true", Beer.class)
                .list();
        listOfBeers = listOfBeers.stream().
                filter(beer -> beer.getTags().stream()
                        .anyMatch(tagche -> tagche.getName().toLowerCase().contains(tag.toLowerCase())))
                .collect(Collectors.toList());

        List<BeerMinimalDto> listOfMinimalBeers = new ArrayList<>();
        listOfMinimalBeers = dtoMapper.createListOfMinimalBeers(listOfBeers);
        return listOfMinimalBeers;
    }

    @Override
    public List<BeerMinimalDto> sortByName() {
        Session session = sessionFactory.getCurrentSession();
        List<BeerMinimalDto> listOfBeers = session.createQuery(
                "select distinct new com.team13.beertag.models.BeerMinimalDto(" +
                        "b.id, b.name,b.description,b.abv, b.style.name, b.originCountry.name, b.brewery.name) " +
                        "from Beer b " +
                        "where b.enabled = true " +
                        "order by b.name", BeerMinimalDto.class).list();
        return listOfBeers;
    }

    @Override
    public List<BeerMinimalDto> sortByAbvAsc() {
        Session session = sessionFactory.getCurrentSession();
        List<BeerMinimalDto> listOfBeers = session.createQuery(
                "select distinct new com.team13.beertag.models.BeerMinimalDto(" +
                        "b.id, b.name,b.description,b.abv, b.style.name, b.originCountry.name, b.brewery.name) " +
                        "from Beer b " +
                        "where b.enabled = true " +
                        "order by b.abv asc", BeerMinimalDto.class).list();
        return listOfBeers;
    }

    @Override
    public List<BeerMinimalDto> sortByAbvDesc() {
        Session session = sessionFactory.getCurrentSession();
        List<BeerMinimalDto> listOfBeers = session.createQuery(
                "select distinct new com.team13.beertag.models.BeerMinimalDto(" +
                        "b.id, b.name,b.description,b.abv, b.style.name, b.originCountry.name, b.brewery.name) " +
                        "from Beer b " +
                        "where b.enabled = true " +
                        "order by b.abv desc", BeerMinimalDto.class).list();
        return listOfBeers;
    }

    //return top 3 beers
    @Override
    public List<BeerMinimalDto> getTopThreeBeers(){
        return sortByRatingDesc().stream().limit(3).collect(Collectors.toList());
    }

    @Override
    public List<BeerMinimalDto> sortByRatingAsc() {
        Session session = sessionFactory.getCurrentSession();
        List<Beer> list = session.
                createQuery("from Beer where enabled = true", Beer.class)
                .list();
        List<BeerMinimalDto> beers = dtoMapper.createListOfMinimalBeers(list);
        addRatingToBeerMinimalDto(beers);
        return beers.stream().sorted(Comparator.comparingDouble(BeerMinimalDto::getRating)).collect(Collectors.toList());

    }

    @Override
    public List<BeerMinimalDto> sortByRatingDesc() {
        Session session = sessionFactory.getCurrentSession();
        List<Beer> list = session.
                createQuery("from Beer where enabled = true", Beer.class)
                .list();
        List<BeerMinimalDto> beers = dtoMapper.createListOfMinimalBeers(list);
        addRatingToBeerMinimalDto(beers);
        return beers.stream().sorted(Comparator.comparingDouble(BeerMinimalDto::getRating).reversed()).collect(Collectors.toList());
    }

    @Override
    public List<Double> getBeerRating(int id) {
        Beer beer = getById(id);
        Session session = sessionFactory.getCurrentSession();
        Query<Double> query = session.createQuery(
                "select avg(rating) from Rating where beerId = :id order by beerId", Double.class);
        query.setParameter("id", id);
        return query.list();
    }

    @Override
    public List<Rating> getListOfRatingsForBeer(int id){
        Beer beer = getById(id);
        Session session = sessionFactory.getCurrentSession();
        Query<Rating> query = session.createQuery(
                "from Rating where beerId = :id order by beerId", Rating.class)
                .setParameter("id", id);
        return query.list();
    }

    @Override
    public Double addBeerRating(int id, Rating rating) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Beer beer = getById(id);
            session.save(rating);
            session.getTransaction().commit();
            return rating.getRating();
        } catch (Exception ex) {
            throw new DuplicateEntityException(String.format(BEER_ALREADY_RATED_BY_USER, id, rating.getBeerId()));
        }
    }

    @Override
    public Set<BeerViewListDto> addRatingToBeerViewListDto(Set<BeerViewListDto> list) {
        Iterator<BeerViewListDto> i = list.iterator();
        while (i.hasNext()) {
            BeerViewListDto beer = i.next();
            Double rating = getBeerRating(beer.getId()).get(0);
            if(rating != null) {
                beer.setRating(getBeerRating(beer.getId()).get(0));
            }
        }
        return list;
    }

    private List<BeerMinimalDto> addRatingForAllBeers(List<BeerMinimalDto> list) {
        Iterator<BeerMinimalDto> i = list.iterator();
        while (i.hasNext()) {
            BeerMinimalDto beer = i.next();
            beer.setRating(getBeerRating(beer.getId()).get(0));
        }
        return list;
    }

    private List<BeerMinimalDto> addRatingToBeerMinimalDto(List<BeerMinimalDto> list) {
        Iterator<BeerMinimalDto> i = list.iterator();
        while (i.hasNext()) {
            BeerMinimalDto beer = i.next();
            beer.setRating(getBeerRating(beer.getId()).get(0));
            if (beer.getRating() == null) {
                i.remove();
            }
        }
        return list;
    }
}
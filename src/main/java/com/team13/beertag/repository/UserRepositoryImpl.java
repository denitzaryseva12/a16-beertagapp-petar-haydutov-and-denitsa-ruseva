package com.team13.beertag.repository;

import com.team13.beertag.exception.DuplicateEntityException;
import com.team13.beertag.exception.EntityNotFoundException;
import com.team13.beertag.models.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import org.hibernate.query.Query;

import java.util.List;
import java.util.Set;

import static com.team13.beertag.constants.UserConstants.*;

@Repository
public class UserRepositoryImpl implements UserRepository {
    private SessionFactory sessionFactory;
    private BeerRepository beerRepository;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory, BeerRepository beerRepository) {
        this.sessionFactory = sessionFactory;
        this.beerRepository = beerRepository;
    }


    @Override
    public User createUser(User user) {
        Session session = sessionFactory.getCurrentSession();
        session.save(user);
        return user;
    }

    @Override
    public List<UserViewDto> getAllUsers() {
        Session session = sessionFactory.getCurrentSession();
        List<UserViewDto> users = session.createQuery(
                "select distinct new com.team13.beertag.models.UserViewDto(" +
                        "u.id, u.username, u.email, u.firstName, u.lastName, u.picture) " +
                        "from User u " +
                        "where u.enabled = true", UserViewDto.class).list();
        return users;
    }

    @Override
    public User updateUser(int id, User user) {
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        User userToUpdate = getUserById(id);
        if (user.getPassword() != null) {
            userToUpdate.setPassword(user.getPassword());
        }
        if (user.getEmail() != null) {
            userToUpdate.setEmail(user.getEmail());
        }
        if (user.getFirstName() != null) {
            userToUpdate.setFirstName(user.getFirstName());
        }
        if (user.getLastName() != null) {
            userToUpdate.setLastName(user.getLastName());
        }
        if (user.getPicture() != null) {
            userToUpdate.setPicture(user.getPicture());
        }
        session.update(userToUpdate);
        session.getTransaction().commit();
        return userToUpdate;
    }

    @Override
    public User deleteUser(int id) {
        User userToUpdate = getUserById(id);
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        userToUpdate.setEnabled(false);
        session.update(userToUpdate);
        session.getTransaction().commit();
        return userToUpdate;
    }

    @Override
    public User getUserById(int id) {
        Session session = sessionFactory.getCurrentSession();
        User user = session.createQuery("from User u where u.id = :id and u.enabled = true", User.class)
                .setParameter("id", id)
                .uniqueResult();
        if (user == null) {
            throw new EntityNotFoundException(String.format(USER_NOT_FOUND, id));
        }
        return user;
    }

    @Override
    public Set<Beer> addBeerToDrunkList(int userId, int beerId) {
        Session session = sessionFactory.getCurrentSession();
        User user = getUserById(userId);
        Beer beer = beerRepository.getById(beerId);
        session.beginTransaction();
        if (user.getDrankBeers().contains(beer)) {
            throw new DuplicateEntityException(String.format(DRANK_LIST_ALREADY_CONTAINS_BEER, beer.getName()));
        }
        user.addBeerToDrankList(beer);
        session.update(user);
        session.getTransaction().commit();
        return user.getDrankBeers();
    }

    @Override
    public Set<Beer> addBeerToWishList(int userId, int beerId) {
        Session session = sessionFactory.getCurrentSession();
        User user = getUserById(userId);
        Beer beer = beerRepository.getById(beerId);
        session.beginTransaction();
        if (user.getWishList().contains(beer)) {
            throw new DuplicateEntityException(String.format(WISHLIST_ALREADY_CONTAINS_BEER, beer.getName()));
        }
        user.addBeerToWishList(beer);
        session.update(user);
        session.getTransaction().commit();
        return user.getWishList();
    }

    @Override
    public boolean checkUsernameExists(String username) {
        return getByUsername(username).size() != 0;
    }

    @Override
    public boolean checkEmailExists(String email) {
        return getByEmail(email).size() != 0;
    }

    @Override
    public List<User> getByUsername(String username) {
        Session session = sessionFactory.getCurrentSession();
        Query<User> query = session.createQuery(
                "from User where username = :username order by id",
                User.class);
        query.setParameter("username", username);
        return query.list();
    }

    @Override
    public List<User> getByEmail(String email) {
        Session session = sessionFactory.getCurrentSession();
        Query<User> query = session.createQuery(
                "from User where email = :email order by id",
                User.class);
        query.setParameter("email", email);
        return query.list();
    }
}

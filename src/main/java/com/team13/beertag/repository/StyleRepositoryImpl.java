package com.team13.beertag.repository;

import com.team13.beertag.exception.EntityNotFoundException;
import com.team13.beertag.models.Style;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static com.team13.beertag.constants.BeerConstants.STYLE_NAME_NOT_FOUND;
import static com.team13.beertag.constants.BeerConstants.STYLE_NOT_FOUND;

@Repository
public class StyleRepositoryImpl implements StyleRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public StyleRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Style> getAll() {
        Session session = sessionFactory.getCurrentSession();
        Query<Style> query = session.createQuery("from Style where enabled = true " +
                "order by id", Style.class);
        return query.list();
    }

    @Override
    public Style create(Style style) {
        try (Session session = sessionFactory.openSession()) {
            session.save(style);
        }
        return style;
    }

    @Override
    public boolean checkStyleExists(String name) {
        return getByStyleName(name).size() != 0;
    }

    public List<Style> getByStyleName(String name) {
        Session session = sessionFactory.getCurrentSession();
        Query<Style> query = session.createQuery(
                "from Style where name = :name order by id",
                Style.class);
        query.setParameter("name", name);
        if (query == null) {
            throw new EntityNotFoundException(String.format(STYLE_NAME_NOT_FOUND, name));
        }
        return query.list();
    }

    @Override
    public void delete(int id) {
        Style styleToUpdate = getStylesById(id);
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        styleToUpdate.setEnabled(false);
        session.update(styleToUpdate);
        session.getTransaction().commit();
    }

    @Override
    public Style update(int id, Style style) {
        Style styleToUpdate = getStylesById(id);
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        if (style.getName() != null) {
            styleToUpdate.setName(style.getName());
        }
        session.update(styleToUpdate);
        session.getTransaction().commit();
        return styleToUpdate;
    }

    @Override
    public Style getStylesById(int id) {
        Session session = sessionFactory.getCurrentSession();
        Style style = session.createQuery("from Style s where s.id = :id", Style.class)
                .setParameter("id", id)
                .uniqueResult();
        if (style == null) {
            throw new EntityNotFoundException(String.format(STYLE_NOT_FOUND, id));
        }
        return style;
    }
}

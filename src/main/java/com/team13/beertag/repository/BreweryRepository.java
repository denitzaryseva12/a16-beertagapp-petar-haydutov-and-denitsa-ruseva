package com.team13.beertag.repository;

import com.team13.beertag.models.Brewery;

import java.util.List;

public interface BreweryRepository {
    List<Brewery> getAllBreweries();

    Brewery getBreweriesById(int id);

    Brewery create(Brewery brewery);

    boolean checkBreweryExists(String name);

    void delete(int id);

    Brewery update(int id, Brewery brewery);
}
package com.team13.beertag.repository;

import com.team13.beertag.models.OriginCountry;

import java.util.List;

public interface OriginCountryRepository {
    List<OriginCountry> getAll();

    OriginCountry getCountryById(int id);

    OriginCountry create(OriginCountry originCountry);

    boolean checkCountryExists(String name);

    void delete(int id);

    OriginCountry update(int id, OriginCountry originCountry);
}

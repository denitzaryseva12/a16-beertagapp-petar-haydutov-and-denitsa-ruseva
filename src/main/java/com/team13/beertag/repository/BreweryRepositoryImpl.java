package com.team13.beertag.repository;

import com.team13.beertag.exception.EntityNotFoundException;
import com.team13.beertag.models.Brewery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static com.team13.beertag.constants.BeerConstants.BREWERY_NAME_NOT_FOUND;
import static com.team13.beertag.constants.BeerConstants.BREWERY_NOT_FOUND;

@Repository
public class BreweryRepositoryImpl implements BreweryRepository {
    private SessionFactory sessionFactory;
    @Autowired
    public BreweryRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Brewery> getAllBreweries() {
        Session session = sessionFactory.getCurrentSession();
        Query<Brewery> query=session.createQuery("from Brewery where enabled = true " +
                "order by id",Brewery.class);
        return query.list();
    }

    @Override
    public Brewery getBreweriesById(int id) {
        Session session = sessionFactory.getCurrentSession();
        Brewery brewery = session.createQuery(
                "from Brewery b where b.id = :id", Brewery.class)
                .setParameter("id", id)
                .uniqueResult();
        if (brewery == null) {
            throw new EntityNotFoundException(String.format(BREWERY_NOT_FOUND, id));
        }
        return brewery;
    }

    @Override
    public Brewery create(Brewery brewery) {
        try (Session session = sessionFactory.openSession()) {
            session.save(brewery);
        }
        return brewery;
    }

    @Override
    public boolean checkBreweryExists(String name) {
        return getByBreweryName(name).size() != 0;
    }

    private List<Brewery> getByBreweryName(String name) {
        Session session = sessionFactory.getCurrentSession();
        Query<Brewery> query = session.createQuery(
                "from Brewery where name = :name order by id",
                Brewery.class);
        query.setParameter("name", name);
        if (query == null) {
            throw new EntityNotFoundException(String.format(BREWERY_NAME_NOT_FOUND, name));
        }
        return query.list();
    }

    @Override
    public void delete(int id) {
        Brewery breweryToUpdate = getBreweriesById(id);
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        breweryToUpdate.setEnabled(false);
        session.update(breweryToUpdate);
        session.getTransaction().commit();
    }

    @Override
    public Brewery update(int id, Brewery brewery) {
        Brewery breweryToUpdate = getBreweriesById(id);
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        if (breweryToUpdate.getName() != null) {
            breweryToUpdate.setName(brewery.getName());
        }
        session.update(breweryToUpdate);
        session.getTransaction().commit();
        return breweryToUpdate;
    }
}

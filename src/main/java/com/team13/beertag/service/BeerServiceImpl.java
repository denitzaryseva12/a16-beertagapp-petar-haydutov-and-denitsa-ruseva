package com.team13.beertag.service;

import com.team13.beertag.exception.DuplicateEntityException;
import com.team13.beertag.models.Beer;
import com.team13.beertag.models.BeerMinimalDto;
import com.team13.beertag.models.BeerViewListDto;
import com.team13.beertag.models.Rating;
import com.team13.beertag.repository.BeerRepository;
import org.omg.CORBA.BAD_PARAM;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import static com.team13.beertag.constants.BeerConstants.BEER_EXISTS_MESSAGE;
import static com.team13.beertag.constants.UserConstants.EMAIL_EXISTS_MESSAGE;

@Service
public class BeerServiceImpl implements BeerService {
    private static final int PAGE_SIZE = 6;
    private BeerRepository repository;

    @Autowired
    public BeerServiceImpl(BeerRepository repository) {
        this.repository = repository;
    }

    @Override
    public Beer create(Beer beer) {
        if (repository.checkBeerExists(beer.getName())) {
            throw new DuplicateEntityException(
                    String.format(BEER_EXISTS_MESSAGE, beer.getName())
            );
        }
        return repository.create(beer);
    }

    @Override
    public List<BeerMinimalDto> getAllWithFilter(String originCountry, String style, String tag) {
        List<BeerMinimalDto> listOfBeers = repository.getAll();
        if (originCountry.isEmpty() && style.isEmpty() && tag.isEmpty()) {
            return listOfBeers;
        }
        if (!originCountry.isEmpty()) {
            return repository.filterByCountry(originCountry);
        }
        if (!style.isEmpty()) {
            return repository.filterByStyle(style);
        }
        if (!tag.isEmpty()) {
            return repository.filterByTag(tag);
        }
        return listOfBeers;
    }

    @Override
    public List<BeerMinimalDto> getAll() {
        List<BeerMinimalDto> listOfBeers = repository.getAll();
        return listOfBeers;
    }

    @Override
    public List<Beer> getBeerByName(String name) {
        return repository.getBeerByName(name);
    }

    @Override
    public Beer getById(int id) {
        return repository.getById(id);
    }

    @Override
    public Beer update(int id, Beer beer) {
        if (repository.checkBeerExists(beer.getName())) {
            if (!(beer.getId() == getBeerByName(beer.getName()).get(0).getId())) {
                throw new DuplicateEntityException(String.format(BEER_EXISTS_MESSAGE, beer.getName()));
            }
        }
        return repository.update(id, beer);
    }

    @Override
    public void delete(int id) {
        repository.delete(id);
    }

    @Override
    public List<BeerMinimalDto> sortBeers(String orderBy) {
        switch (orderBy.toLowerCase()) {
            case "name":
                return repository.sortByName();
            case "abvasc":
                return repository.sortByAbvAsc();
            case "abvdesc":
                return repository.sortByAbvDesc();
            case "ratingasc":
                return repository.sortByRatingAsc();
            case "ratingdesc":
                return repository.sortByRatingDesc();
            default:
                throw new BAD_PARAM(String.format("Unable to order by %s", orderBy));
        }
    }

    @Override
    public Double getBeerRating(int id) {
        List<Double> rating = repository.getBeerRating(id);
        return rating.get(0);
    }

    @Override
    public List<Rating> getListOfRatingsForBeer(int id) {
        return repository.getListOfRatingsForBeer(id);
    }


    @Override
    public Double addBeerRating(int id, Rating rating) {
        repository.addBeerRating(id, rating);
        return getBeerRating(id);
    }

    @Override
    public BeerMinimalDto displayBeers(Beer beer) {
        BeerMinimalDto beerToDisplay = new BeerMinimalDto();
        beerToDisplay.setName(beer.getName());
        if (beer.getPicture() != null) {
            beerToDisplay.setPicture(beer.getPicture());
        } else beerToDisplay.setPicture("default.picture"); // TODO insert default.picture
        beerToDisplay.setAbv(beer.getAbv());
        beerToDisplay.setDescription(beer.getDescription());
        return beerToDisplay;
    }

    @Override
    public Page<BeerMinimalDto> getBeerPaged(Pageable pageable,
                                             String sortBy, String country, String style, String tag) {
        List<BeerMinimalDto> beers;
        if (!style.isEmpty() || !country.isEmpty()) {
            beers = getAllWithFilter(country, style, tag);
        } else {
            beers = getAll();
        }
        sortBeers(sortBy);
        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List<BeerMinimalDto> beerMinimalDtoList;
        if (beers.size() < startItem) {
            beerMinimalDtoList = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, beers.size());
            beerMinimalDtoList = beers.subList(startItem, toIndex);
        }
        return new PageImpl<BeerMinimalDto>(beerMinimalDtoList, PageRequest.of(currentPage, pageSize), beers.size());
    }

    @Override
    public List<BeerMinimalDto> getTopThreeBeers() {
        return repository.getTopThreeBeers();
    }

    @Override
    public Set<BeerViewListDto> addRatingToBeerViewListDto(Set<BeerViewListDto> list) {
        return repository.addRatingToBeerViewListDto(list);
    }
}


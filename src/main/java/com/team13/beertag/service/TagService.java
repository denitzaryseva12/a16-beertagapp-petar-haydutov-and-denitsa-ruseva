package com.team13.beertag.service;

import com.team13.beertag.models.Tag;

import java.util.List;

public interface TagService {
    List<Tag> getAll();

    Tag getTagById(int id);

    Tag create(Tag tag);

    Tag delete(int id);

    Tag update(int id, Tag tag);

    List<Tag> filterByName(String name);

    Tag getByName(String name);
}

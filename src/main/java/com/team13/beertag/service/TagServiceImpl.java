package com.team13.beertag.service;

import com.team13.beertag.exception.DuplicateEntityException;
import com.team13.beertag.models.Tag;
import com.team13.beertag.repository.TagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.team13.beertag.constants.BeerConstants.TAG_EXISTS_MESSAGE;

//TODO add validation for adding new Tags
@Service
public class TagServiceImpl implements TagService {
    private TagRepository tagRepository;

    @Autowired
    public TagServiceImpl(TagRepository tagRepository) {
        this.tagRepository = tagRepository;
    }

    @Override
    public List<Tag> getAll() {
        return tagRepository.getAll();
    }

    @Override
    public List<Tag> filterByName(String name) {
        return tagRepository.filterByName(name);
    }

    @Override
    public Tag getByName(String name) {
        return tagRepository.getTagByName(name);
    }

    @Override
    public Tag getTagById(int id) {
        return tagRepository.getTagById(id);
    }

    @Override
    public Tag create(Tag tag) {
        if (tagRepository.checkTagExists(tag.getName())) {
            throw new DuplicateEntityException(
                    String.format(TAG_EXISTS_MESSAGE, tag.getName())
            );
        }
        return tagRepository.createTag(tag);
    }

    @Override
    public Tag update(int id, Tag tag) {
        if (tagRepository.checkTagExists(tag.getName())) {
            throw new DuplicateEntityException(String.format(TAG_EXISTS_MESSAGE, tag.getName()));
        }
        return tagRepository.updateTag(id, tag);
    }

    @Override
    public Tag delete(int id) {
        return tagRepository.deleteTag(id);
    }
}

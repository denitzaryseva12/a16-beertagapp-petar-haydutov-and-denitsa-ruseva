package com.team13.beertag.service;

import com.team13.beertag.models.OriginCountry;

import java.util.List;

public interface OriginCountryService {
    List<OriginCountry> getAll();

    OriginCountry getById(int id);

    OriginCountry create(OriginCountry originCountry);

    void delete(int id);

    OriginCountry update(int id, OriginCountry originCountry);
}

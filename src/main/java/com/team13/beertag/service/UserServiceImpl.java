package com.team13.beertag.service;

import com.team13.beertag.exception.DuplicateEntityException;
import com.team13.beertag.models.*;
import com.team13.beertag.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.team13.beertag.constants.UserConstants.EMAIL_EXISTS_MESSAGE;
import static com.team13.beertag.constants.UserConstants.USERNAME_EXISTS_MESSAGE;


@Service
public class UserServiceImpl implements UserService {
    private UserRepository userRepository;
    private BeerService beerService;
    private DtoMapper dtoMapper;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, DtoMapper dtoMapper, BeerService beerService) {
        this.userRepository = userRepository;
        this.dtoMapper = dtoMapper;
        this.beerService = beerService;
    }

    @Override
    public User createUser(User user) {
        if (userRepository.checkUsernameExists(user.getUsername())) {
            throw new DuplicateEntityException(
                    String.format(USERNAME_EXISTS_MESSAGE, user.getUsername()));
        }
        if (userRepository.checkEmailExists(user.getEmail())) {
            throw new DuplicateEntityException(
                    String.format(EMAIL_EXISTS_MESSAGE, user.getEmail()));
        }
        return userRepository.createUser(user);
    }

    @Override
    public List<UserViewDto> getAllUsers() {
        return userRepository.getAllUsers();
    }

    @Override
    public User updateUser(int id, User user) {
        if (userRepository.checkEmailExists(user.getEmail())) {
            if(!(user.getId() == userRepository.getByEmail(user.getEmail()).get(0).getId())){
                throw new DuplicateEntityException(String.format(EMAIL_EXISTS_MESSAGE, user.getEmail()));
            }
        }
        if (userRepository.checkUsernameExists(user.getUsername())) {
            if(!(user.getId() == userRepository.getByUsername(user.getUsername()).get(0).getId())) {
                throw new DuplicateEntityException(String.format(USERNAME_EXISTS_MESSAGE, user.getUsername()));
            }
        }
        return userRepository.updateUser(id, user);
    }

    @Override
    public User deleteUser(int id) {
        return userRepository.deleteUser(id);
    }

    @Override
    public UserProfileDto getUserById(int id) {
        User user = userRepository.getUserById(id);
        UserProfileDto userProfileDto = new UserProfileDto();

        userProfileDto.setId(user.getId());
        userProfileDto.setUsername(user.getUsername());
        userProfileDto.setEmail(user.getEmail());
        userProfileDto.setFirstName(user.getFirstName());
        userProfileDto.setLastName(user.getLastName());
        userProfileDto.setPicture(user.getPicture());

        userProfileDto.setDrankBeers(dtoMapper.createSetOfMinimalBeers(user.getDrankBeers()));
        userProfileDto.setWishList(dtoMapper.createSetOfMinimalBeers(user.getWishList()));
        return userProfileDto;
    }

    @Override
    public Set<BeerViewListDto> getUserDrunkList(int id) {
        User user = userRepository.getUserById(id);
        Set<BeerViewListDto> set = dtoMapper.createSetOfMinimalBeers(user.getDrankBeers());
        beerService.addRatingToBeerViewListDto(set);
        return set;
    }

    @Override
    public Set<BeerViewListDto> getUserWishList(int id) {
        User user = userRepository.getUserById(id);
        Set<BeerViewListDto> set = dtoMapper.createSetOfMinimalBeers(user.getWishList());
        beerService.addRatingToBeerViewListDto(set);
        return set;
    }

    @Override
    public Set<BeerViewListDto> addBeerToDrunkList(int userId, int beerId) {
        Set<Beer> set = userRepository.addBeerToDrunkList(userId, beerId);
        return dtoMapper.createSetOfMinimalBeers(set);
    }

    @Override
    public Set<BeerViewListDto> addBeerToWishList(int userId, int beerId) {
        Set<Beer> set = userRepository.addBeerToWishList(userId, beerId);
        return dtoMapper.createSetOfMinimalBeers(set);
    }

    @Override
    public User getByUsername(String username){
        return userRepository.getByUsername(username).get(0);
    }
}

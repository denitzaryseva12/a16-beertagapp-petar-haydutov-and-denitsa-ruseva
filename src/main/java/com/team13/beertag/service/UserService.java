package com.team13.beertag.service;

import com.team13.beertag.models.*;

import java.util.List;
import java.util.Set;

public interface UserService {
    User createUser(User user);

    List<UserViewDto> getAllUsers();

    User updateUser(int id, User user);

    User deleteUser(int id);

    UserProfileDto getUserById(int id);

    Set<BeerViewListDto> getUserDrunkList(int id);

    Set<BeerViewListDto> getUserWishList(int id);

    Set<BeerViewListDto> addBeerToDrunkList(int userId, int beerId);

    Set<BeerViewListDto> addBeerToWishList(int userId, int beerId);

    User getByUsername(String username);
}

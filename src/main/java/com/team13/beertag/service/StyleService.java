package com.team13.beertag.service;

import com.team13.beertag.models.Style;

import java.util.List;

public interface StyleService {
    List<Style> getAllStyles();

    Style getStylesById(int id);

    Style create(Style style);

    void delete(int id);

    Style update(int id, Style style);

    List<Style> getByStyleName(String name);
}

package com.team13.beertag.service;

import com.team13.beertag.exception.DuplicateEntityException;
import com.team13.beertag.models.Brewery;
import com.team13.beertag.repository.BreweryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.team13.beertag.constants.BeerConstants.BEER_EXISTS_MESSAGE;
import static com.team13.beertag.constants.BeerConstants.BREWERY_EXISTS_MESSAGE;

@Service
public class BreweryServiceImpl implements BreweryService {
    private BreweryRepository breweryRepository;

    @Autowired
    public BreweryServiceImpl(BreweryRepository breweryRepository) {
        this.breweryRepository = breweryRepository;
    }

    @Override
    public List<Brewery> getAllBreweries() {
        return breweryRepository.getAllBreweries();
    }

    @Override
    public Brewery getBreweriesById(int id) {
        return breweryRepository.getBreweriesById(id);
    }

    @Override
    public Brewery create(Brewery brewery) {
        if (breweryRepository.checkBreweryExists(brewery.getName())) {
            throw new DuplicateEntityException(
                    String.format(BREWERY_EXISTS_MESSAGE, brewery.getName()));
        }
        return breweryRepository.create(brewery);
    }

    @Override
    public void delete(int id) {
        breweryRepository.delete(id);
    }

    @Override
    public Brewery update(int id, Brewery brewery) {
        if (breweryRepository.checkBreweryExists(brewery.getName())) {
            throw new DuplicateEntityException(
                    String.format(BEER_EXISTS_MESSAGE, brewery.getName()));
        }
        return breweryRepository.update(id, brewery);
    }
}

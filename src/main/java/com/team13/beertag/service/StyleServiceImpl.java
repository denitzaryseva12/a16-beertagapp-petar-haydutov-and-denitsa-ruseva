package com.team13.beertag.service;

import com.team13.beertag.exception.DuplicateEntityException;
import com.team13.beertag.models.Style;
import com.team13.beertag.repository.StyleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.team13.beertag.constants.BeerConstants.BEER_EXISTS_MESSAGE;
import static com.team13.beertag.constants.BeerConstants.STYLE_EXISTS_MESSAGE;

@Service
public class StyleServiceImpl implements StyleService {
    private StyleRepository styleRepository;

    @Autowired
    public StyleServiceImpl(StyleRepository styleRepository) {
        this.styleRepository = styleRepository;
    }

    @Override
    public List<Style> getAllStyles() {
        return styleRepository.getAll();
    }

    @Override
    public Style getStylesById(int id) {
        return styleRepository.getStylesById(id);
    }

    @Override
    public Style create(Style style) {
        if (styleRepository.checkStyleExists(style.getName())) {
            throw new DuplicateEntityException(String.format(STYLE_EXISTS_MESSAGE, style.getName()));
        }
        return styleRepository.create(style);
    }

    @Override
    public void delete(int id) {
        styleRepository.delete(id);
    }

    @Override
    public Style update(int id, Style style) {
        if (styleRepository.checkStyleExists(style.getName())) {
            throw new DuplicateEntityException(
                    String.format(BEER_EXISTS_MESSAGE, style.getName()));

        }
        return styleRepository.update(id, style);
    }

    @Override
    public List<Style> getByStyleName(String name) {
        return styleRepository.getByStyleName(name);
    }
}

package com.team13.beertag.service;

import com.team13.beertag.exception.DuplicateEntityException;
import com.team13.beertag.models.OriginCountry;
import com.team13.beertag.repository.OriginCountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.team13.beertag.constants.BeerConstants.ORIGIN_COUNTRY_EXISTS_MESSAGE;

@Service
public class OriginCountryServiceImpl implements OriginCountryService {
    private OriginCountryRepository originCountryRepository;

    @Autowired
    public OriginCountryServiceImpl(OriginCountryRepository originCountryRepository) {
        this.originCountryRepository = originCountryRepository;
    }

    @Override
    public List<OriginCountry> getAll() {
        return originCountryRepository.getAll();
    }

    @Override
    public OriginCountry getById(int id) {
        return originCountryRepository.getCountryById(id);
    }

    @Override
    public OriginCountry create(OriginCountry originCountry) {
        if (originCountryRepository.checkCountryExists(originCountry.getName())) {
            throw new DuplicateEntityException(
                    String.format(ORIGIN_COUNTRY_EXISTS_MESSAGE, originCountry.getName())
            );
        }
        return originCountryRepository.create(originCountry);
    }

    @Override
    public void delete(int id) {
        originCountryRepository.delete(id);
    }

    @Override
    public OriginCountry update(int id, OriginCountry originCountry) {
        if (originCountryRepository.checkCountryExists(originCountry.getName())) {
            throw new DuplicateEntityException(
                    String.format(ORIGIN_COUNTRY_EXISTS_MESSAGE, originCountry.getName()));
        }
        return originCountryRepository.update(id, originCountry);
    }
}

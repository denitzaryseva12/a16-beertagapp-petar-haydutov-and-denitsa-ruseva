package com.team13.beertag.service;

import com.team13.beertag.models.Brewery;

import java.util.List;

public interface BreweryService {
    List<Brewery> getAllBreweries();

    Brewery getBreweriesById(int id);

    Brewery create(Brewery brewery);

    void delete(int id);

    Brewery update(int id, Brewery brewery);
}

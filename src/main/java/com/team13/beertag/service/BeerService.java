package com.team13.beertag.service;

import com.team13.beertag.models.Beer;
import com.team13.beertag.models.BeerMinimalDto;
import com.team13.beertag.models.BeerViewListDto;
import com.team13.beertag.models.Rating;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Set;

public interface BeerService {
    Beer create(Beer beer);

    List<BeerMinimalDto> getAllWithFilter(String originCountry, String style, String tag);

    List<BeerMinimalDto> getAll();

    List<Beer> getBeerByName(String name);

    Beer getById(int id);

    Beer update(int id, Beer beer);

    void delete(int id);

    List<BeerMinimalDto> sortBeers(String orderBy);

    Double getBeerRating(int id);

    Double addBeerRating(int id, Rating rating);

    BeerMinimalDto displayBeers(Beer beer);

    Page<BeerMinimalDto> getBeerPaged(Pageable pageable, String sortBy, String country, String style, String tag);

    Set<BeerViewListDto> addRatingToBeerViewListDto(Set<BeerViewListDto> list);

    List<Rating> getListOfRatingsForBeer(int id);

    List<BeerMinimalDto> getTopThreeBeers();
}


package com.team13.beertag.controller.rest;

import com.team13.beertag.exception.DuplicateEntityException;
import com.team13.beertag.exception.EntityNotFoundException;
import com.team13.beertag.models.Style;
import com.team13.beertag.service.StyleService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/styles")
public class StyleRestController {
    private StyleService service;

    public StyleRestController(StyleService service) {
        this.service = service;
    }

    @PostMapping
    public Style create(@RequestBody @Valid Style style) {
        try {
            Style styleToCreate = new Style();
            styleToCreate.setName(style.getName());
            return service.create(styleToCreate);
        } catch (DuplicateEntityException dee) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, dee.getMessage());
        }catch (EntityNotFoundException dee) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, dee.getMessage());
        }
    }

    @GetMapping
    public List<Style> getAll() {
        List<Style> styles = service.getAllStyles();
        return styles;
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        try {
            service.delete(id);
        } catch (EntityNotFoundException dee) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, dee.getMessage());
        }
    }

    @GetMapping("/{id}")
    public Style getStylesById(@PathVariable int id) {
        try {
            return service.getStylesById(id);
        } catch (EntityNotFoundException dee) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, dee.getMessage());
        }
    }

    @PutMapping("{id}")
    public Style update(@PathVariable int id, @RequestBody Style style) {
        try {
            return service.update(id, style);
        } catch (DuplicateEntityException dee) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, dee.getMessage());
        } catch (EntityNotFoundException dee) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, dee.getMessage());
        }
    }
}

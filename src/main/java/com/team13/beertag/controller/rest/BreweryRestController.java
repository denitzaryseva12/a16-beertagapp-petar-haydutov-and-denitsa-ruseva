package com.team13.beertag.controller.rest;

import com.team13.beertag.exception.DuplicateEntityException;
import com.team13.beertag.exception.EntityNotFoundException;
import com.team13.beertag.models.Brewery;
import com.team13.beertag.service.BreweryService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/breweries")
public class BreweryRestController {
    private BreweryService service;

    public BreweryRestController(BreweryService service) {
        this.service = service;
    }

    @PostMapping
    public Brewery create(@RequestBody @Valid Brewery brewery) {
        try {
            Brewery breweryToCreate = new Brewery();
            breweryToCreate.setName(brewery.getName());
            return service.create(brewery);
        } catch (DuplicateEntityException dee) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, dee.getMessage());
        } catch (EntityNotFoundException dee) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, dee.getMessage());
        }
    }

    @GetMapping
    public List<Brewery> getAll() {
        List<Brewery> breweries = service.getAllBreweries();
        return breweries;
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        try {
            service.delete(id);
        } catch (EntityNotFoundException dee) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, dee.getMessage());
        }
    }

    @GetMapping("/{id}")
    public Brewery getBreweriesById(@PathVariable int id) {
        try {
            return service.getBreweriesById(id);
        } catch (EntityNotFoundException dee) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, dee.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Brewery update(@PathVariable int id, @RequestBody Brewery brewery) {
        try {
            return service.update(id, brewery);
        } catch (DuplicateEntityException dee) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, dee.getMessage());
        } catch (EntityNotFoundException dee) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, dee.getMessage());
        }
    }
}

package com.team13.beertag.controller.rest;

import com.team13.beertag.exception.DuplicateEntityException;
import com.team13.beertag.exception.EntityNotFoundException;
import com.team13.beertag.helper.BeerCollectionHelper;
import com.team13.beertag.models.*;
import com.team13.beertag.service.BeerService;
import org.omg.CORBA.BAD_PARAM;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/beers")
public class BeerRestController {
    private BeerService service;
    private DtoMapper dtoMapper;

    @Autowired
    public BeerRestController(BeerService service, DtoMapper dtoMapper) {
        this.service = service;
        this.dtoMapper = dtoMapper;
    }

    @PostMapping
    public Beer create(@RequestBody @Valid BeerDto beerDto) {
        try {
            Beer newBeer = dtoMapper.fromBeerDto(beerDto);
            return service.create(newBeer);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        try {
            service.delete(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public Beer getById(@PathVariable int id) {
        try {
            return service.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Beer update(@PathVariable int id, @RequestBody @Valid BeerDto beerDto) {
        try {
            Beer newBeer = dtoMapper.fromBeerDto(beerDto);
            return service.update(id, newBeer);
        } catch (DuplicateEntityException dee) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, dee.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/filter")
    @ResponseBody
    public List<BeerMinimalDto> getAllWithFilter(@RequestParam(defaultValue = "") String originCountry,
                                       @RequestParam(defaultValue = "") String style,
                                       @RequestParam(defaultValue = "") String tag) {
        try {
            List<BeerMinimalDto> beers = service.getAllWithFilter(originCountry, style, tag);
            return beers;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping()
    public List<BeerMinimalDto> getAll() {
        try {
            List<BeerMinimalDto> beers = service.getAll();
            return beers;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/sort")
    @ResponseBody
    //TODO sort by Rating
    public List<BeerMinimalDto> sort(@RequestParam String orderBy) {
        try {
            return service.sortBeers(orderBy);
        } catch (BAD_PARAM bp) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, bp.getMessage());
        }
    }

    @GetMapping("/{id}/rating")
    public Double getBeerRating(@PathVariable int id) {
        try {
            return service.getBeerRating(id);
        } catch (EntityNotFoundException enfEx) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enfEx.getMessage());
        }
    }

    @PostMapping("/{id}/rating")
    public Double addBeerRating(@PathVariable int id,
                                @RequestBody Rating rating) {
        try {
            return service.addBeerRating(id, rating);
        } catch (EntityNotFoundException enfEx) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enfEx.getMessage());
        } catch (DuplicateEntityException dee) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, dee.getMessage());
        }
    }
}
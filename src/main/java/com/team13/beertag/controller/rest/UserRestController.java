package com.team13.beertag.controller.rest;

import com.team13.beertag.exception.DuplicateEntityException;
import com.team13.beertag.exception.EntityNotFoundException;
import com.team13.beertag.models.*;
import com.team13.beertag.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/api/users")
public class UserRestController {
    private UserService userService;
    private DtoMapper dtoMapper;

    @Autowired
    public UserRestController(UserService userService, DtoMapper dtoMapper) {
        this.userService = userService;
        this.dtoMapper = dtoMapper;
    }

    @PostMapping
    public User create(@RequestBody @Valid UserRegDto userRegDto) {
        try {
            User user = dtoMapper.fromUserDto(userRegDto);
            return userService.createUser(user);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @GetMapping
    public List<UserViewDto> getAllUsers() {
        return userService.getAllUsers();
    }

    @GetMapping("/{id}")
    public UserProfileDto getUserById(@PathVariable int id) {
        try {
            return userService.getUserById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException dee) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, dee.getMessage());
        }
    }

    @PutMapping("/{id}")
    public User updateUser(@PathVariable int id, @RequestBody @Valid User user) {
        try {
            return userService.updateUser(id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException dee) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, dee.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public User deleteUser(@PathVariable Integer id) {
        try {
            return userService.deleteUser(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{id}/drunk-list")
    public Set<BeerViewListDto> getUserDrunkList(@PathVariable Integer id) {
        try {
            return userService.getUserDrunkList(id);
        } catch (EntityNotFoundException enfe) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enfe.getMessage());
        }
    }

    @GetMapping("/{id}/wish-list")
    public Set<BeerViewListDto> getUserWishList(@PathVariable Integer id) {
        try {
            return userService.getUserWishList(id);
        } catch (EntityNotFoundException enfe) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enfe.getMessage());
        }
    }

    @PutMapping("/{userId}/drunk-list/{beerId}")
    public Set<BeerViewListDto> addBeerToDrunkList(@PathVariable Integer userId, @PathVariable Integer beerId) {
        try {
            return userService.addBeerToDrunkList(userId, beerId);
        } catch (EntityNotFoundException enfe) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enfe.getMessage());
        } catch (DuplicateEntityException dee) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, dee.getMessage());
        }
    }

    @PutMapping("/{userID}/wish-list/{beerID}")
    public Set<BeerViewListDto> addBeerToWishList(@PathVariable Integer userID, @PathVariable Integer beerID) {
        try {
            return userService.addBeerToWishList(userID, beerID);
        } catch (EntityNotFoundException enfe) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enfe.getMessage());
        } catch (DuplicateEntityException dee) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, dee.getMessage());
        }
    }

}

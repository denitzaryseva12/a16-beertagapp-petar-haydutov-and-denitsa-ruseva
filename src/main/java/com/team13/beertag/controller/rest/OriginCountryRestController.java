package com.team13.beertag.controller.rest;

import com.team13.beertag.exception.DuplicateEntityException;
import com.team13.beertag.exception.EntityNotFoundException;
import com.team13.beertag.models.OriginCountry;
import com.team13.beertag.service.OriginCountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/countries")
public class OriginCountryRestController {
    private OriginCountryService service;

    @Autowired
    public OriginCountryRestController(OriginCountryService service) {
        this.service = service;
    }

    @PostMapping
    public OriginCountry create(@RequestBody @Valid OriginCountry originCountry) {
        try {
            OriginCountry originCountryToCreate = new OriginCountry();
            return service.create(originCountry);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (EntityNotFoundException dee) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, dee.getMessage());
        }
    }

    @GetMapping
    public List<OriginCountry> getAll() {
        List<OriginCountry> countries = service.getAll();
        return countries;
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        try {
            service.delete(id);
        } catch (EntityNotFoundException enfe) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enfe.getMessage());
        }
    }

    @GetMapping("/{id}")
    public OriginCountry getById(@PathVariable int id) {
        try {
            return service.getById(id);
        } catch (EntityNotFoundException enfe) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enfe.getMessage());
        }
    }

    @PutMapping("/{id}")
    public OriginCountry update(@PathVariable int id, @RequestBody OriginCountry originCountry) {
        try {
            return service.update(id, originCountry);
        } catch (DuplicateEntityException dee) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, dee.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}

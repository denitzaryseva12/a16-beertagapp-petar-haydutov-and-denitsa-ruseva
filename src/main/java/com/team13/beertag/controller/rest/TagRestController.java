package com.team13.beertag.controller.rest;

import com.team13.beertag.exception.DuplicateEntityException;
import com.team13.beertag.exception.EntityNotFoundException;
import com.team13.beertag.models.DtoMapper;
import com.team13.beertag.models.Tag;
import com.team13.beertag.service.TagService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/tags")
public class TagRestController {
    private TagService service;

    public TagRestController(TagService service) {
        this.service = service;
    }

    @GetMapping
    public List<Tag> getAll(@RequestParam(defaultValue = "") String name) {
        if (name.isEmpty()) {
            return service.getAll();
        } else {
            return service.filterByName(name);
        }
    }

    @GetMapping("/{id}")
    public Tag getById(@PathVariable int id) {
        try {
            return service.getTagById(id);
        } catch (EntityNotFoundException dee) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, dee.getMessage());
        }
    }

    @PostMapping
    public Tag createTag(@RequestBody Tag tag) {
        try {
            Tag tagToCreate = new Tag();
            tagToCreate.setName(tag.getName());
            return service.create(tagToCreate);
        } catch (DuplicateEntityException dee) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, dee.getMessage());
        } catch (EntityNotFoundException dee) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, dee.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public Tag deleteTag(@PathVariable int id) {
        try {
            return service.delete(id);
        } catch (EntityNotFoundException dee) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, dee.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Tag updateTag(@PathVariable int id, @RequestBody Tag tag) {
        try {
            return service.update(id, tag);
        } catch (DuplicateEntityException dee) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, dee.getMessage());
        } catch (EntityNotFoundException dee) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, dee.getMessage());
        }
    }

}

package com.team13.beertag.controller;

import com.team13.beertag.exception.DuplicateEntityException;
import com.team13.beertag.exception.EntityNotFoundException;
import com.team13.beertag.helper.UserHelper;
import com.team13.beertag.models.*;
import com.team13.beertag.service.BeerService;
import com.team13.beertag.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.spel.SpelEvaluationException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


import javax.validation.Valid;
import java.util.Base64;
import java.util.Set;

@Controller
public class UserController {
    private UserService userService;
    private DtoMapper dtoMapper;
    private BeerService beerService;

    @Autowired
    public UserController(UserService userService, DtoMapper dtoMapper, BeerService beerService) {
        this.userService = userService;
        this.dtoMapper = dtoMapper;
        this.beerService = beerService;
    }

    @GetMapping("/users")
    public String showAllUsers(Model model) {
        model.addAttribute("users", userService.getAllUsers());
        return "users";
    }

    //TODO fix return type to be UserProfileDto
    @GetMapping("/users/{username}")
    @PreAuthorize("authentication.principal.username == #username or hasAuthority('ADMIN')")
    public String showUserProfile(@PathVariable String username, Model model) {
        try {
            User user = userService.getByUsername(username);
            UserViewDto userViewDto = dtoMapper.fromUserToUserViewDto(user);

            model.addAttribute("user", userViewDto);
            return "user-profile";
        } catch (SpelEvaluationException see) {
            return "redirect:/access-denied";
        }
    }

    @GetMapping("/users/{username}/beer-list")
    @PreAuthorize("authentication.principal.username == #username or hasAuthority('ADMIN')")
    public String showUserBeerList(@PathVariable String username, Model model) {
        try {
            User user = userService.getByUsername(username);
            Set<BeerViewListDto> beersDrinkList = userService.getUserDrunkList(user.getId());
            Set<BeerViewListDto> beersWishList = userService.getUserWishList(user.getId());

            model.addAttribute("user", user);
            model.addAttribute("beersDrinkList", beersDrinkList);
            model.addAttribute("beersWishList", beersWishList);
            model.addAttribute("beerDto", new BeerDto());
            return "user-beer-list";
        }  catch (EntityNotFoundException enfe) {
            return "error";
        }
    }

    @PostMapping("/users/{username}/beer-list/add-beer-to-wish-list")
    public String addBeerToWishList(@PathVariable String username, Model model, BeerDto beerDto) {
        try {
            User user = userService.getByUsername(username);
            Beer beer = beerService.getById(beerDto.getId());

            userService.addBeerToWishList(user.getId(), beer.getId());

            return showUserBeerList(username, model);
        } catch (DuplicateEntityException dee) {
            return "error";
        } catch (EntityNotFoundException enfe) {
            return "error";
        }
    }

    @PostMapping("/users/{username}/beer-list/add-beer-to-drink-list")
    public String addBeerToDrinkList(@PathVariable String username, Model model, BeerDto beerDto) {
        try {
            User user = userService.getByUsername(username);
            Beer beer = beerService.getById(beerDto.getId());

            userService.addBeerToDrunkList(user.getId(), beer.getId());

            return showUserBeerList(username, model);
        } catch (DuplicateEntityException dee) {
            return "error";
        } catch (EntityNotFoundException enfe) {
            return "error";
        }
    }

    @GetMapping("/users/{username}/edit")
    @PreAuthorize("authentication.principal.username == #username or hasAuthority('ADMIN')")
    public String showUpdateForm(@PathVariable String username, Model model) {
        try {
            User user = userService.getByUsername(username);
            UserViewDto userViewDto = dtoMapper.fromUserToUserViewDto(user);

            model.addAttribute("user", userViewDto);
            return "update-user";
        } catch (EntityNotFoundException enfe) {
            return "error";
        }
    }

    @PostMapping("/users/{username}/update")
    public String updateUser(@PathVariable String username, @RequestParam MultipartFile file,
                             @Valid UserViewDto userViewDto, BindingResult bindingResult,
                             Model model) throws Exception {
        if (bindingResult.hasErrors()) {
            String errorMsg = "";
            if (bindingResult.getFieldError() != null) {
                errorMsg = bindingResult.getFieldError().getDefaultMessage();
            }
            model.addAttribute("error", errorMsg);
            return "update-user";
        }

        try {
            User user = userService.getByUsername(userViewDto.getUsername());
            UserHelper.updateUserDetails(user, userViewDto);
            if (!file.isEmpty()) {
                user.setPicture(Base64.getEncoder().encodeToString(file.getBytes()));
            }

            userService.updateUser(user.getId(), user);
            return showUserProfile(user.getUsername(), model);
        } catch (DuplicateEntityException dee) {
            return "error";
        } catch (EntityNotFoundException enfe) {
            return "error";
        }
    }

    @GetMapping("/users/{username}/delete")
    @PreAuthorize("authentication.principal.username == #username or hasAuthority('ADMIN')")
    public String showUserDeletePage(@PathVariable String username, Model model) {
        try {
            User user = userService.getByUsername(username);
            model.addAttribute("user", user);
            return "user-delete";
        } catch (EntityNotFoundException enfe) {
            return "error";
        }
    }

    @PostMapping("/users/{username}/delete")
    @PreAuthorize("authentication.principal.username == #username or hasAuthority('ADMIN')")
    public String deleteUser(@PathVariable String username, Model model) {
        try {
            User user = userService.getByUsername(username);
            userService.deleteUser(user.getId());
            model.addAttribute("user", user);
            return "user-delete-success";
        } catch (EntityNotFoundException enfe) {
            return "error";
        }
    }
}
package com.team13.beertag.controller;

import com.team13.beertag.exception.DuplicateEntityException;
import com.team13.beertag.exception.EntityNotFoundException;
import com.team13.beertag.helper.BeerCollectionHelper;
import com.team13.beertag.models.*;
import com.team13.beertag.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.expression.spel.SpelEvaluationException;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Controller
public class BeerController {

    private BeerService beerService;
    private StyleService styleService;
    private OriginCountryService originCountryService;
    private BreweryService breweryService;
    private UserService userService;
    private DtoMapper dtoMapper;
    private TagService tagService;

    @Autowired
    public BeerController(BeerService beerService, StyleService styleService,
                          OriginCountryService originCountryService, BreweryService breweryService,
                          UserService userService, TagService tagService,
                          DtoMapper dtoMapper) {
        this.beerService = beerService;
        this.styleService = styleService;
        this.originCountryService = originCountryService;
        this.breweryService = breweryService;
        this.userService = userService;
        this.dtoMapper = dtoMapper;
        this.tagService = tagService;
    }

    @GetMapping("/beers")
    public String showBeers(@RequestParam(name = "style", defaultValue = "") String style,
                            @RequestParam(name = "country", defaultValue = "") String country,
                            @RequestParam(name = "tag", defaultValue = "") String tag,
                            @RequestParam(name = "page", defaultValue = "1") int currentPage,
                            @RequestParam(name = "size", defaultValue = "6") int pageSize,
                            @RequestParam(required = false, defaultValue = "name") String sortBy,
                            Model model) {
        Page<BeerMinimalDto> beerPage = beerService.getBeerPaged(PageRequest.of(currentPage - 1, pageSize),
                sortBy, country, style, tag);
        model.addAttribute("beerPage", beerPage);

        List<String> listOfFilterNames = new ArrayList<>();
        listOfFilterNames.add("style");
        listOfFilterNames.add("country");
        listOfFilterNames.add("tag");
        model.addAttribute("listOfFilterNames", listOfFilterNames);

        List<BeerMinimalDto> beers;
        if (!style.isEmpty() || !country.isEmpty()) {
            beers = beerService.getAllWithFilter(country, style, tag);
        } else {
            beers = beerService.getAll();
        }
        model.addAttribute("beers", beers);
        int totalPages = beerPage.getTotalPages();
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
            model.addAttribute("pageNumbers", pageNumbers);
        }
        return "beers";
    }

    @GetMapping("/beers/{id}")
    public String showBeerDetails(@PathVariable int id, Model model) {
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            //checks if current user is admin
            boolean hasUserRole = authentication.getAuthorities().stream()
                    .anyMatch(r -> r.getAuthority().equals("ADMIN"));

            String username = authentication.getName();

            User user = userService.getByUsername(username);
            Beer beer = beerService.getById(id);
            BeerMinimalDto beerMinimalDto = dtoMapper.fromBeerToMinimalDto(beer);

            beerMinimalDto.setRating(beerService.getBeerRating(id));
            List<Rating> listOfRatings = beerService.getListOfRatingsForBeer(id);
            List<Integer> listOfCreatorId = new ArrayList<>();
            for (Rating rating : listOfRatings) {
                listOfCreatorId.add(rating.getUserId());
            }
            beerMinimalDto.setListOfRaterId(listOfCreatorId);

            model.addAttribute("beerMinimalDto", beerMinimalDto);
            model.addAttribute("user", user);
            model.addAttribute("hasUserRole", hasUserRole);
            model.addAttribute("currentBeerId", id);
            return "beer-details";
        } catch (EntityNotFoundException enfe) {
            return "redirect:/beers";
        }
    }

    @PostMapping("/beers/{id}/rate")
    public String rateBeer(@PathVariable int id, Model model, BeerMinimalDto beerMinimalDto
            , BindingResult errors) {
        if (errors.hasErrors()) {
            String errorMsg = "";
            if (errors.getFieldError() != null) {
                errorMsg = errors.getFieldError().getDefaultMessage();
            }
            return "beer-details";
        }

        try {
            Beer beer = beerService.getById(id);
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            String username = authentication.getName();
            User user = userService.getByUsername(username);
            Rating rating = new Rating();

            rating.setRating(beerMinimalDto.getRating());
            rating.setUserId(user.getId());
            rating.setBeerId(beer.getId());

            beerService.addBeerRating(id, rating);
            return showBeerDetails(id, model);
        } catch (EntityNotFoundException enfe) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enfe.getMessage());
        } catch (DuplicateEntityException dee) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, dee.getMessage());
        }
    }

    @ModelAttribute("/styles")
    public List<Style> populateStyles() {
        return styleService.getAllStyles();
    }

    @ModelAttribute("/countries")
    public List<OriginCountry> populateCountry() {
        return originCountryService.getAll();
    }

    @ModelAttribute("/breweries")
    public List<Brewery> populateBrewery() {
        return breweryService.getAllBreweries();
    }

    @ModelAttribute("/tags")
    public List<Tag> populateTag() {
        return tagService.getAll();
    }

    @GetMapping("/beers/new")
    public String showNewBeerForm(Model model) {
        model.addAttribute("beer", new BeerDto());
        model.addAttribute("styles", styleService.getAllStyles());
        model.addAttribute("countries", originCountryService.getAll());
        model.addAttribute("breweries", breweryService.getAllBreweries());
        model.addAttribute("allTags", tagService.getAll());
        return "beer-create";
    }

    @PostMapping("/beers/new")
    public String createBeer(@RequestParam MultipartFile file,
                             @Valid @ModelAttribute("beer") BeerDto beer,
                             BindingResult errors, Model model) throws Exception {
        if (errors.hasErrors()) {
            String errorMsg = "";
            if (errors.getFieldError() != null) {
                errorMsg = errors.getFieldError().getDefaultMessage();
            }
            return "beer-create";
        }

        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            String username = authentication.getName();
            User user = userService.getByUsername(username);

            Beer toCreate = new Beer();
            toCreate.setStyle(styleService.getStylesById(beer.getStyleId()));
            toCreate.setOriginCountry(originCountryService.getById(beer.getOriginCountryId()));
            toCreate.setBrewery(breweryService.getBreweriesById(beer.getBreweryId()));
            toCreate.setAbv(beer.getAbv());
            toCreate.setName(beer.getName());
            toCreate.setUserId(user.getId());
            toCreate.addTag(tagService.getByName(beer.getTags().get(0)));
            if (!file.isEmpty()) {
                toCreate.setPicture(Base64.getEncoder().encodeToString(file.getBytes()));
            }

            beerService.create(toCreate);
            return "redirect:/beers";
        } catch (DuplicateEntityException dee) {
            model.addAttribute("error", "Beer already exists");
            return showNewBeerForm(model);
        }
    }

    @GetMapping("/beers/update/{id}")
    public String showEditBeerForm(@PathVariable("id") int id, Model model, Principal principal) {
        try {
            Beer beer = beerService.getById(id);
            BeerDto beerDto = dtoMapper.fromBeerToDto(beer);

            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            String username = authentication.getName();
            User user = userService.getByUsername(username);

            boolean hasUserRole = authentication.getAuthorities().stream()
                    .anyMatch(r -> r.getAuthority().equals("ADMIN"));

            if (user.getId() != beer.getUserId() && !hasUserRole) {
                return "redirect:/access-denied";
            }

            model.addAttribute("beerDto", beerDto);
            model.addAttribute("styles", styleService.getAllStyles());
            model.addAttribute("countries", originCountryService.getAll());
            model.addAttribute("breweries", breweryService.getAllBreweries());
            model.addAttribute("allTags", tagService.getAll());
            return "beers-update";
        } catch (EntityNotFoundException enfe) {
            return "error";
        } catch (DuplicateEntityException dee) {
            return "error";
        }
    }

    @PostMapping("/beers/update/{id}")
    public String updateBeer(@PathVariable("id") @ModelAttribute("beer") int id, @RequestParam MultipartFile file,
                             BeerDto beer, BindingResult bindingResult, Model model) throws Exception {
        if (bindingResult.hasErrors()) {
            String errorMsg = "";
            if (bindingResult.getFieldError() != null) {
                errorMsg = bindingResult.getFieldError().getDefaultMessage();
            }
            model.addAttribute("error", errorMsg);
            return "beers-update";
        }
        try {
            Beer beerToUpdate = beerService.getById(id);
            BeerCollectionHelper.updateBeerDetails(beerToUpdate, beer, tagService);

            beerToUpdate.setStyle(styleService.getStylesById(beer.getStyleId()));
            beerToUpdate.setBrewery(breweryService.getBreweriesById(beer.getBreweryId()));
            beerToUpdate.setOriginCountry(originCountryService.getById(beer.getOriginCountryId()));
            if (!file.isEmpty()) {
                beerToUpdate.setPicture(Base64.getEncoder().encodeToString(file.getBytes()));
            }

            beerService.update(beer.getId(), beerToUpdate);
            model.addAttribute("beer", beerService.getById(id));
            return "redirect:/beers";
        } catch (EntityNotFoundException enfe) {
            return "error";
        } catch (DuplicateEntityException dee) {
            return "error";
        }
    }

    @GetMapping("/beers/delete/{id}")
    public String showDeleteBeerForm(@PathVariable("id") int id, Model model, Principal principal) {
        try {
            Beer beer = beerService.getById(id);
            model.addAttribute("beer", beer);
            return "beer-delete";
        } catch (EntityNotFoundException enfe){
            return "error";
        }
    }


    @PostMapping("/beers/delete/{id}")
    public String deleteBeer(@PathVariable("id") int id,
                             Beer beer, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            String errorMsg = "";
            if (bindingResult.getFieldError() != null) {
                errorMsg = bindingResult.getFieldError().getDefaultMessage();
            }
            model.addAttribute("error", errorMsg);
            return "beer-delete";
        }
        try {
            beerService.delete(id);
            model.addAttribute("beer", beerService.getAll());
            return "redirect:/beers";
        } catch (EntityNotFoundException enfe){
            return "error";
        }
    }

    @GetMapping("/beers/top")
    public String showTopThreeBeers(Model model) {
        List<BeerMinimalDto> listOfTopBeers = beerService.getTopThreeBeers();

        model.addAttribute("listOfTopBeers", listOfTopBeers);

        return "beers-top";
    }
}


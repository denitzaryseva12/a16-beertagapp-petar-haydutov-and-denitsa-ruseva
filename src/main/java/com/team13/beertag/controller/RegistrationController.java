package com.team13.beertag.controller;

import com.team13.beertag.models.User;
import com.team13.beertag.models.UserRegDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.List;

@Controller
public class RegistrationController {
    private UserDetailsManager userDetailsManager;
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    public RegistrationController(UserDetailsManager userDetailsManager, BCryptPasswordEncoder passwordEncoder) {
        this.userDetailsManager = userDetailsManager;
        this.passwordEncoder = passwordEncoder;
    }

    @GetMapping("/register")
    public String showRegisterPage(Model model) {
        model.addAttribute("userRegDto", new UserRegDto());
        return "register";
    }

    @PostMapping("/register")
    public String registerUser(@Valid @ModelAttribute UserRegDto userRegDto, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            String error = "";
            if (bindingResult.getFieldError() != null) {
                error = bindingResult.getFieldError().getDefaultMessage();
            } else if (bindingResult.getGlobalError() != null) {
                error = bindingResult.getGlobalError().getDefaultMessage()
                        .replace(bindingResult.getGlobalError().getDefaultMessage(),
                                "Passwords do not match.");
            }

            model.addAttribute("error", error);
            return "register";
        }

        if (userDetailsManager.userExists(userRegDto.getUsername())) {
            model.addAttribute("error", "User with this username already exists!");
            return "register";
        }

        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("USER");
        org.springframework.security.core.userdetails.User newUser =
                new org.springframework.security.core.userdetails.User(
                        userRegDto.getUsername(),
                        passwordEncoder.encode(userRegDto.getPassword()),
                        authorities);
        userDetailsManager.createUser(newUser);
        return "register-confirmation";
    }

    @GetMapping("/register-confirmation")
    public String showRegisterConfirmation() {
        return "register-confirmation";
    }
}

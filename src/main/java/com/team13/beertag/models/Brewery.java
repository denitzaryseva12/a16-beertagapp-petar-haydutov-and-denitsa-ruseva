package com.team13.beertag.models;


import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import static com.team13.beertag.constants.BeerConstants.*;

@Entity
@Table(name = "breweries")
public class Brewery {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @Positive
    private int id;

    @Column(name = "name")
    @NotNull
    @Size(min = BREWERY_NAME_MIN_LENGTH,
            max = BREWERY_NAME_MAX_LENGTH,
            message = BREWERY_INVALID_NAME_LENGTH_ERROR_MSG)
    private String name;

    @Column(name = "enabled")
    private boolean enabled;

    public Brewery() {
    }

    public Brewery(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}

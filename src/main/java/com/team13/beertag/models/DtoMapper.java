package com.team13.beertag.models;

import com.team13.beertag.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class DtoMapper {
    private StyleRepository styleRepository;
    private BreweryRepository breweryRepository;
    private OriginCountryRepository originCountryRepository;
    private TagRepository tagRepository;

    @Autowired
    public DtoMapper(StyleRepository styleRepository,
                     BreweryRepository breweryRepository,
                     OriginCountryRepository originCountryRepository,
                     TagRepository tagRepository) {
        this.styleRepository = styleRepository;
        this.breweryRepository = breweryRepository;
        this.originCountryRepository = originCountryRepository;
        this.tagRepository = tagRepository;
    }

    public Beer fromBeerDto(BeerDto beerDto) {
        Beer beer = new Beer(beerDto.getName(), beerDto.getDescription(), beerDto.getAbv());
        Style style = styleRepository.getStylesById(beerDto.getStyleId());
        Brewery brewery = breweryRepository.getBreweriesById(beerDto.getBreweryId());
        OriginCountry originCountry = originCountryRepository.getCountryById(beerDto.getOriginCountryId());

        List<Tag> tags = new ArrayList<>();
        for (String s : beerDto.getTags()) {
            Tag tag = new Tag(s);
            tags.add(tag);
            tagRepository.createTag(tag);
        }

        return beer;
    }

    public User fromUserDto(UserRegDto userRegDto) {
        User user = new User();
        user.setUsername(userRegDto.getUsername());
        user.setPassword(userRegDto.getPassword());

        return user;
    }

    public UserViewDto fromUserToUserViewDto(User user) {
        UserViewDto userViewDto = new UserViewDto();

        userViewDto.setId(user.getId());
        userViewDto.setUsername(user.getUsername());
        userViewDto.setEmail(user.getEmail());
        userViewDto.setFirstName(user.getFirstName());
        userViewDto.setLastName(user.getLastName());
        userViewDto.setPicture(user.getPicture());

        return userViewDto;
    }

    public BeerDto fromBeerToDto(Beer beer) {
        BeerDto beerDto = new BeerDto();

        beerDto.setId(beer.getId());
        beerDto.setAbv(beer.getAbv());
        beerDto.setName(beer.getName());
        beerDto.setDescription(beer.getDescription());
        beerDto.setPicture(beer.getPicture());
        beerDto.setBreweryId(beer.getBrewery().getId());
        beerDto.setStyleId(beer.getStyle().getId());
        beerDto.setOriginCountryId(beer.getOriginCountry().getId());

        return beerDto;
    }

    public BeerMinimalDto fromBeerToMinimalDto(Beer beer) {
        BeerMinimalDto beerMinimalDto = new BeerMinimalDto();

        beerMinimalDto.setName(beer.getName());
        beerMinimalDto.setId(beer.getId());
        beerMinimalDto.setAbv(beer.getAbv());
        beerMinimalDto.setDescription(beer.getDescription());
        beerMinimalDto.setStyleName(beer.getStyle().getName());
        beerMinimalDto.setBreweryName(beer.getBrewery().getName());
        beerMinimalDto.setOriginCountryName(beer.getOriginCountry().getName());
        beerMinimalDto.setPicture(beer.getPicture());
        beerMinimalDto.setCreatorId(beer.getUserId());

        for (Tag tag: beer.getTags()) {
            beerMinimalDto.addTag(tag.getName());
        }

        return beerMinimalDto;
    }

    public Set<BeerViewListDto> createSetOfMinimalBeers(Set<Beer> setOfBeers) {
        Set<BeerViewListDto> setOfMinimalBeers = new HashSet<>();
        for (Beer beer : setOfBeers) {
            BeerViewListDto beerMinimalDto = new BeerViewListDto(beer.getId() ,beer.getName(),
                    beer.getAbv(), beer.getStyle().getName(), beer.getPicture());

            setOfMinimalBeers.add(beerMinimalDto);
        }
        return setOfMinimalBeers;
    }

    public List<BeerMinimalDto> createListOfMinimalBeers(List<Beer> listOfBeers) {
        List<BeerMinimalDto> listOfMinimalBeers = new ArrayList<>();
        for (Beer beer : listOfBeers) {
            BeerMinimalDto beerMinimalDto = new BeerMinimalDto(beer.getId(), beer.getName(), beer.getDescription(),
                    beer.getAbv(), beer.getStyle().getName(), beer.getOriginCountry().getName(),
                    beer.getBrewery().getName());

            listOfMinimalBeers.add(beerMinimalDto);
        }
        return listOfMinimalBeers;
    }
}

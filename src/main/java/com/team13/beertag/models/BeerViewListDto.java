package com.team13.beertag.models;

public class BeerViewListDto {
    private int id;
    private String name;
    private double abv;
    private String style;
    private String picture;
    private Double rating;

    public BeerViewListDto() {
    }

    public BeerViewListDto(int id ,String name, double abv, String style, String picture) {
        this.id=id;
        this.name = name;
        this.abv = abv;
        this.style = style;
        this.picture = picture;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getAbv() {
        return abv;
    }

    public void setAbv(double abv) {
        this.abv = abv;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}

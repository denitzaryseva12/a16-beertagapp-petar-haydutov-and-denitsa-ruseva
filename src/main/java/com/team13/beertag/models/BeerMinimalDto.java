package com.team13.beertag.models;

import java.util.ArrayList;
import java.util.List;

public class BeerMinimalDto {
    private int id;
    private String name;
    private double abv;
    private String description;
    private List<String> tags;
    private String styleName;
    private String originCountryName;
    private String breweryName;
    private Double rating;
    private String picture;
    private List<Integer> listOfRaterId;
    private int creatorId;

    public BeerMinimalDto() {
        tags = new ArrayList<>();
        listOfRaterId = new ArrayList<>();
    }

    public BeerMinimalDto(String name, String description, double abv) {
        this.name = name;
        this.abv = abv;
        this.description = description;
        tags = new ArrayList<>();
    }

    public BeerMinimalDto(int id, String name, String description, double abv, String styleName,
                          String originCountryName, String breweryName, String picture) {
        this.id = id;
        this.name = name;
        this.abv = abv;
        this.description = description;
        this.styleName = styleName;
        this.originCountryName = originCountryName;
        this.breweryName = breweryName;
        this.picture = picture;
        tags = new ArrayList<>();
        listOfRaterId = new ArrayList<>();
    }

    public BeerMinimalDto(int id, String name, String description, double abv, String styleName,
                          String originCountryName, String breweryName) {
        this.id = id;
        this.name = name;
        this.abv = abv;
        this.description = description;
        this.styleName = styleName;
        this.originCountryName = originCountryName;
        this.breweryName = breweryName;
        tags = new ArrayList<>();
        listOfRaterId = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public double getAbv() {
        return abv;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAbv(double abv) {
        this.abv = abv;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public void addTag(String tag){
        tags.add(tag);
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStyleName() {
        return styleName;
    }

    public void setStyleName(String styleName) {
        this.styleName = styleName;
    }

    public String getOriginCountryName() {
        return originCountryName;
    }

    public void setOriginCountryName(String originCountryName) {
        this.originCountryName = originCountryName;
    }

    public String getBreweryName() {
        return breweryName;
    }

    public void setBreweryName(String breweryName) {
        this.breweryName = breweryName;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public List<Integer> getListOfRaterId() {
        return listOfRaterId;
    }

    public void setListOfRaterId(List<Integer> listOfRaterId) {
        this.listOfRaterId = listOfRaterId;
    }

    public int getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(int creatorId) {
        this.creatorId = creatorId;
    }
}
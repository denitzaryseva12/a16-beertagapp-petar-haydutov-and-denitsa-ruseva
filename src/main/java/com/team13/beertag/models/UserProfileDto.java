package com.team13.beertag.models;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class UserProfileDto {

    private int id;

    private String username;

    private String email;

    private String firstName;

    private String lastName;

    private String picture;

    private List<Role> roles;

    private Set<BeerViewListDto> drankBeers;

    private Set<BeerViewListDto> wishList;

    public UserProfileDto() {
        roles = new ArrayList<>();
        drankBeers = new HashSet<>();
        wishList = new HashSet<>();
    }

    public UserProfileDto(String username, String email) {
        this.username = username;
        this.email = email;
        roles = new ArrayList<>();
        drankBeers = new HashSet<>();
        wishList = new HashSet<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public Set<BeerViewListDto> getDrankBeers() {
        return drankBeers;
    }

    public void setDrankBeers(Set<BeerViewListDto> drankBeers) {
        this.drankBeers = drankBeers;
    }

    public Set<BeerViewListDto> getWishList() {
        return wishList;
    }

    public void setWishList(Set<BeerViewListDto> wishList) {
        this.wishList = wishList;
    }
}

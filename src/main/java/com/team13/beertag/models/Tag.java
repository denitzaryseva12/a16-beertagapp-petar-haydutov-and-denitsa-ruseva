package com.team13.beertag.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import static com.team13.beertag.constants.BeerConstants.*;

@Entity
@Table(name = "tags")
public class Tag {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @Positive
    private int id;

    @Column(name = "name")
    @NotNull
    @Size(min = TAG_NAME_MIN_LENGTH,
            max = TAG_NAME_MAX_LENGTH,
            message = TAG_INVALID_NAME_LENGTH_ERROR_MSG)
    private String name;

    @Column(name = "enabled")
    private boolean enabled;

    public Tag() {
    }

    public Tag(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String content) {
        this.name = content;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}

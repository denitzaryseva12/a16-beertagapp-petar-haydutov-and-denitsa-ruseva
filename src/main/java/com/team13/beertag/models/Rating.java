package com.team13.beertag.models;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

import static com.team13.beertag.constants.BeerConstants.*;

@Entity
@Table(name = "beers_users_ratings")
public class Rating {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "beer_rating_id")
    @PositiveOrZero
    private int beerRatingId;

    @Column(name = "beer_id")
    @PositiveOrZero
    private int beerId;

    @Column(name = "user_id")
    @PositiveOrZero
    private int userId;

    @Column(name = "rating")
    @PositiveOrZero
    @Min(value = 0, message = "Rating should not be less than 0.")
    @Max(value = 5,message = "Rating should not be less than 5.")
    private Double rating;

    public int getBeerRatingId() {
        return beerRatingId;
    }

    public void setBeerRatingId(int beerRatingId) {
        this.beerRatingId = beerRatingId;
    }

    public int getBeerId() {
        return beerId;
    }

    public void setBeerId(int beerId) {
        this.beerId = beerId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }
}


package com.team13.beertag.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.transaction.Transactional;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.team13.beertag.constants.UserConstants.*;

@Entity
@Table(name = "users")
@Transactional
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    @PositiveOrZero
    private int id;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    @JsonIgnore
    private String password;

    @Column(name = "email")
    private String email;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @ManyToMany
    @JoinTable(
            name = "users_drunk_beers",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "beer_id")
    )
    private Set<Beer> drankBeers;

    @ManyToMany
    @JoinTable(
            name = "users_wished_beers",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "beer_id")
    )
    private Set<Beer> wishList;

    @Column(name = "enabled")
    @JsonIgnore
    private boolean enabled;

    @Lob
    @Column(name = "picture_url")
    private String picture;

    public User() {
        drankBeers = new HashSet<>();
        wishList = new HashSet<>();
    }

    public User(String username, String email) {
        this.username = username;
        this.email = email;
        drankBeers = new HashSet<>();
        wishList = new HashSet<>();
    }

    public int getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Set<Beer> getDrankBeers() {
        return drankBeers;
    }

    public Set<Beer> getWishList() {
        return wishList;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setDrankBeers(Set<Beer> drankBeer) {
        this.drankBeers = drankBeer;
    }

    public void addBeerToDrankList(Beer beer) {
        drankBeers.add(beer);
    }

    public void setWishList(Set<Beer> wishList) {
        this.wishList = wishList;
    }

    public void addBeerToWishList(Beer beer) {
        wishList.add(beer);
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }


    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}

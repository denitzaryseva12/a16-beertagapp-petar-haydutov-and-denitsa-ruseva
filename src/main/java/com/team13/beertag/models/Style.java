package com.team13.beertag.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import static com.team13.beertag.constants.BeerConstants.*;

@Entity
@Table(name = "styles")
public class Style {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @Positive
    private int id;

    @Column(name = "name")
    @NotNull
    @Size(min = STYLE_NAME_MIN_LENGTH,
            max = STYLE_NAME_MAX_LENGTH,
            message = STYLE_INVALID_NAME_LENGTH_ERROR_MSG)
    private String name;

    @Column(name = "enabled")
    private boolean enabled;

    public Style() {
    }

    public Style(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}

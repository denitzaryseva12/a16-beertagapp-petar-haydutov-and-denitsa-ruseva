package com.team13.beertag.models;

import jdk.nashorn.internal.objects.annotations.ScriptClass;
import org.hibernate.validator.constraints.ScriptAssert;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

import static com.team13.beertag.constants.UserConstants.*;

@ScriptAssert(lang = "javascript", alias = "_",
        script = "_.passwordConfirmation != null && _.passwordConfirmation.equals(_.password)")
public class UserRegDto {
    @NotNull
    @NotBlank
    @Size(min = USER_NAME_MIN_LENGTH,
            max = USER_NAME_MAX_LENGTH,
            message = USERNAME_INVALID_LENGTH_ERROR_MSG)
    private String username;

    @NotNull
    @NotBlank
    @Size(min = PASSWORD_MIN_LENGTH,
            max = PASSWORD_MAX_LENGTH,
            message = PASSWORD_INVALID_LENGTH_ERROR_MSG)
    private String password;

    private String passwordConfirmation;

    public UserRegDto() {
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }
}

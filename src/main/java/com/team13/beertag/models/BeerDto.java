package com.team13.beertag.models;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

import java.util.ArrayList;
import java.util.List;

import static com.team13.beertag.constants.BeerConstants.*;

public class BeerDto {
    @PositiveOrZero(message = POSITIVE_BEER_ID)
    private int id;

    @NotNull
    @Size(min = BEER_NAME_MIN_LENGTH,
            max = BEER_NAME_MAX_LENGTH,
            message = BEER_INVALID_LENGTH_ERROR_MSG)
    private String name;

    @PositiveOrZero(message = POSITIVE_STYLE_ID)
    private int styleId;

    @PositiveOrZero(message = POSITIVE_BREWERY_ID)
    private int breweryId;

    @PositiveOrZero(message = POSITIVE_ORIGIN_COUNTRY_ID)
    private int originCountryId;

    @PositiveOrZero
    private double abv;

    @Size(min = DESCRIPTION_NAME_MIN_LENGTH,
            max = DESCRIPTION_NAME_MAX_LENGTH,
            message = DESCRIPTION_INVALID_LENGTH_ERROR_MSG)
    private String description;

    private String picture;

    private List<String> tags;

    public BeerDto() {
        tags = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<String> getTags() {
        return tags;
}

    public void setTags(List<String> newTags) {
        tags.addAll(newTags);
    }

    public void addTag(String tag){
        tags.add(tag);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getOriginCountryId() {
        return originCountryId;
    }

    public void setOriginCountry(int originCountryId) {
        this.originCountryId = originCountryId;
    }

    public int getBreweryId() {
        return breweryId;
    }

    public void setBrewery(int breweryId) {
        this.breweryId = breweryId;
    }

    public int getStyleId() {
        return styleId;
    }

    public void setStyleId(int styleId) {
        this.styleId = styleId;
    }

    public double getAbv() {
        return abv;
    }

    public void setAbv(double abv) {
        this.abv = abv;
    }

    public void setBreweryId(int breweryId) {
        this.breweryId = breweryId;
    }

    public void setOriginCountryId(int originCountryId) {
        this.originCountryId = originCountryId;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}

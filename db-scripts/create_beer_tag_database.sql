DROP SCHEMA IF EXISTS `beer_tag`;
create database if not exists beer_tag;
use beer_tag;

create table styles
(
    id   int(10)     not null auto_increment,
    name      varchar(50) not null unique,
    enabled    boolean not null default true,
    primary key (id),
    check (length(Name) > 2)
);

create table breweries
(
    id int(10)     not null auto_increment,
    name      varchar(50) not null unique,
    enabled    boolean not null default true,
    primary key (id),
    check (length(name) > 2)
);

create table origin_countries
(
    id int(10)     not null auto_increment,
    name            varchar(50) not null unique,
    enabled    boolean not null default true,
    primary key (id),
    check (length(name) > 3)
);

create table tags
(
    id     int(10)     not null auto_increment,
    name      varchar(50) not null unique,
    enabled    boolean not null default true,
    primary key (id),
    check (length(name) > 2)
);

create table users
(
    id     int(10)     not null auto_increment,
    username   varchar(50) not null unique,
    password   varchar(68) not null,
    email      varchar(50) unique,
    enabled    boolean not null default true,
    first_name  varchar(30),
    last_name   varchar(30),
    picture_url longblob,
    primary key (username),
    index (id),
    check (length(username) > 3),
    check (length(password) > 5),
    check (length(email) > 7),
    check (length(first_name) > 1),
    check (length(last_name) > 1)
);

create table authorities
(
    username varchar(50) not null,
    authority varchar(50) not null,
    unique key Username_Authority (username, authority),
    constraint authorities_fk foreign key (username) references users(username)
);

create table beers
(
    id          int(10)     not null auto_increment,
    name            varchar(50) not null unique,
    description     varchar(500),
    abv             double      not null,
    style_id         int(10)     not null,
    brewery_id       int(10)     not null,
    country_id int(10)     not null,
    user_id          int(10)     not null,
    picture_url longblob,
    enabled    boolean not null default true,
    primary key (id),
    foreign key (style_id) references styles (id),
    foreign key (brewery_id) references breweries (id),
    foreign key (country_id) references origin_countries (id),
    foreign key (user_id) references users (id),
    check (length(name) > 1),
    check (length(description) > 20)
);

create table beers_tags
(
    beer_tag_id int(10) not null auto_increment,
    tag_id     int(10) not null,
    beer_id    int(10) not null,
    primary key (beer_tag_id),
    foreign key (tag_id) references tags (id),
    foreign key (beer_id) references beers (id)
);

create table users_drunk_beers
(
    users_drunk_beers_id int(10) not null auto_increment,
    user_id          int(10) not null,
    beer_id          int(10) not null,
    primary key (users_drunk_beers_id),
    foreign key (user_id) references users (id),
    foreign key (beer_id) references beers (id),
    constraint unique (user_id, beer_id)
);

create table users_wished_beers
(
    users_wished_beers_id int(10) not null auto_increment,
    user_id           int(10) not null,
    beer_id           int(10) not null,
    primary key (users_wished_beers_id),
    foreign key (user_id) references users (id),
    foreign key (beer_id) references beers (id),
    constraint unique (user_id, beer_id)
);

create table beers_users_ratings
(
    beer_rating_id int(10) not null auto_increment,
    beer_id       int(10) not null,
    user_id       int(10) not null,
    rating       int(1)  not null,
    primary key (beer_rating_id),
    foreign key (beer_id) references beers (id),
    foreign key (user_id) references users (id),
    check (rating > 0 and rating < 6),
    constraint unique (user_id, beer_id)
);
Trello link - https://trello.com/b/PDgNgkTn

http://localhost:8080/swagger-ui.html#/

The goal of the project was to create a RESTful multi-layered web application
while also implementing MVC controllers for the required different
functionalities. The assignment required us to create an accessible catalogue
of beers that provided uses of different access levels different functionalities
with additional features such as creation, removal and editing of beers and user
information.The project used Hibernate, Bootstrap, the Spring framework, MariaDb and
Thymeleaf in order to integrate the different layers of the project,
create an adequate and working visual representation of the business
functionality that handles different requests by the clients accessing the
application. We did not use a an already-created template and build the html
and css files from scratch.

